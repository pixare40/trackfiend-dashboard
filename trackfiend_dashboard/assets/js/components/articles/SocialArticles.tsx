import ArticlesState from '../common/state/ArticlesState';
import Articles from './Articles';
import ArticlesStore from '../../stores/ArticlesStore';
import ArticleModel from '../common/state/ArticlesModel';

export default class SocialArticles extends Articles {
    getArticles(): ArticleModel[]{
        return ArticlesStore.getSocialArticles();
    }

    getArticleType(): string {
        return "social";
    }
}