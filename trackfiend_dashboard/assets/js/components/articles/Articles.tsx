import * as React from 'react';
import { RouteComponentProps, Link } from 'react-router-dom';
import { ComponentBase } from 'resub';
import ArticlesState from '../common/state/ArticlesState';
import { Alert, List, Avatar, Row, Col, Button, Spin, Pagination } from 'antd';
import ArticleModel from '../common/state/ArticlesModel';
import { Mutation, Query } from 'react-apollo';
import { ArticleRequestsValueObject } from '../../constants/ArticleRequestsValueObject';

export default abstract class Articles extends ComponentBase<{}, ArticlesState>{
    protected currentArticles = {};

    constructor(props) {
        super(props);
        this.currentArticles = this.getArticles();
    }

    abstract getArticleType(): string;
    abstract getArticles(): ArticleModel[]

    render(): JSX.Element {

        return (
            <div>
                {this.getAddNewComponent()}
                <Row className="root-row">
                    <Query query={ArticleRequestsValueObject.GET_ARTICLE_BY_TYPE}
                        variables={{ type: this.getArticleType(), page: this.state.currentPage }}>
                        {({ loading, error, data }) => {
                            if (loading) return <Spin size="small" />
                            if (error) return
                            (<div>
                                <Row className="root-row">
                                    <Alert
                                        message={this.getMessage()}
                                        description="There are currently no articles available"
                                        type="info"
                                        showIcon
                                    />
                                </Row>
                            </div>)

                            let articles = data.getArticlesByType.articles;
                            let totalCount = data.getArticlesByType.count

                            return (
                                <div>
                                    <List
                                        itemLayout="horizontal"
                                        dataSource={data.getArticlesByType.articles}
                                        renderItem={(item: ArticleModel) => (
                                            <List.Item actions={[<Link to={"/articles/" + this.getArticleType() + "/edit/" + item.id}>edit</Link>,
                                            <Link to={"/articles/" + this.getArticleType() + "/view/" + item.id}>more</Link>]}>
                                                <List.Item.Meta
                                                    avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                                                    title={<a href={"/articles/" + this.getArticleType() + "/view/" + item.id}>{item.title}</a>}
                                                    description={item.summary}
                                                />
                                            </List.Item>
                                        )}
                                    />
                                    <Pagination total={ totalCount }
                                        defaultPageSize={ 10 }
                                        current = { this.state.currentPage }
                                        onChange={ (page, pageSize)=>{
                                            this.setState({...{currentPage: page}});
                                        } }
                                        itemRender={
                                            (current, type, originalElement)=>{
                                                if (type === 'prev') {
                                                    return <a>Previous</a>;
                                                }
                                                if (type === 'next') {
                                                    return <a>Next</a>;
                                                }
                                                return originalElement; 
                                            }
                                        }/>
                                </div>
                            )
                        }}
                    </Query>
                </Row>
            </div>

        );
    }

    capitalizeFirstLetter(string): string {
        if (string == null) {
            return "";
        }
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    getMessage = (): string => {
        return "No " + this.capitalizeFirstLetter(this.getArticleType()) + " Articles";
    }

    getAddNewComponent(): JSX.Element {
        return (
            <Row gutter={16} className="root-row">
                <Col span={4}>
                    <Link to={"/articles/" + this.getArticleType() + "/create"}>
                        <Button icon="plus" size="large" type="primary">Add New</Button>
                    </Link>
                </Col>
            </Row>
        )
    }
}