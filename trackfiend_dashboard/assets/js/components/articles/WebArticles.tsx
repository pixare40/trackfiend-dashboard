import ArticlesState from '../common/state/ArticlesState';
import Articles from './Articles';
import ArticlesStore from '../../stores/ArticlesStore';
import ArticleModel from '../common/state/ArticlesModel';

export default class WebArticles extends Articles {
    protected _buildState(props: {}, initialState: true): ArticlesState{
        return{
            articleType: "web",
            articles: ArticlesStore.getWebArticles(),
            currentPage: 1
        }
    }

    getArticles(): ArticleModel[]{
        return ArticlesStore.getWebArticles();
    }

    getArticleType(): string {
        return "web";
    }
}