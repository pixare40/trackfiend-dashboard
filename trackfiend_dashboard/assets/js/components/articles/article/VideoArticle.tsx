import * as React from 'react';
import { ComponentBase } from 'resub';
import { RouteComponentProps } from 'react-router-dom';
import Article from './Article';
import ArticleModel from '../../common/state/ArticlesModel';
import ArticlesStore from '../../../stores/ArticlesStore';
import CreateArticle from './web/CreateArticle';
import ViewArticle from './ViewArticle';

export default class VideoArticle extends Article {
    getCreateArticleComponent(): JSX.Element {
        return <CreateArticle
            routeProps={this._routeProps}
            onSubmit={this.onSubmit} type="video" />
    }

    getEditArticleComponent(): JSX.Element {
        return (
            <CreateArticle routeProps={this._routeProps}
                onSubmit={this.onSubmit}
                 type="video" />
        )
    }
    
    getViewArticleComponent(): JSX.Element {
        return (
            <ViewArticle type="video" routeProps={ this._routeProps }/>
        )
    }
}