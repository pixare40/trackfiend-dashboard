import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import ArticleModel from '../../common/state/ArticlesModel';
import ArticleState from '../../common/state/ArticleState';
import ArticlesStore from '../../../stores/ArticlesStore';

export default abstract class Article extends React.Component<any, ArticleState>{
    
    protected _articleId;
    protected _action: string;
    protected _routeProps: RouteComponentProps<any>

    constructor(props){
        super(props);
        this._routeProps = this.props as RouteComponentProps<any>;
        this._articleId = this._routeProps.match.params.id;
        this._action = this._routeProps.match.params.action;       
    }

    abstract getEditArticleComponent(): JSX.Element;
    abstract getViewArticleComponent(): JSX.Element;
    abstract getCreateArticleComponent(): JSX.Element;
    
    onSubmit = (articleDetails: ArticleModel) =>{
        ArticlesStore.storeArticle(articleDetails);
        let currentProps = this.props as RouteComponentProps<{}>;
        currentProps.history.push("/articles/web");
    }
    
    render(): JSX.Element{
        if(this._action == "edit"){
            return this.getEditArticleComponent();
        }
        else if(this._action == "create"){
            return this.getCreateArticleComponent();
        }

        return this.getViewArticleComponent();
    }
}