import * as React from 'react';
import Article from './Article';
import ArticleModel from '../../common/state/ArticlesModel';
import CreateArticle from './web/CreateArticle';
import ViewArticle from './ViewArticle';
import { RouteComponentProps } from 'react-router';

export default class WebArticle extends Article {

    getCreateArticleComponent(): JSX.Element {
        return <CreateArticle 
                    routeProps={ this._routeProps } 
                    onSubmit={ this.onSubmit } type="web"/>
    }

    getEditArticleComponent(): JSX.Element {
        return (
            <CreateArticle routeProps={ this._routeProps } 
                onSubmit={ this.onSubmit } 
                type="web"/>
        )
    }

    getViewArticleComponent(): JSX.Element {
        return (
            <ViewArticle type="web" routeProps={ this._routeProps }/>
        )
    }
}