import * as React from 'react';
import ArticleModel from '../../common/state/ArticlesModel';
import { Query } from 'react-apollo';
import { RouteComponentProps, Link } from 'react-router-dom';
import { Spin, Row, Button } from 'antd';
import { ArticleRequestsValueObject } from '../../../constants/ArticleRequestsValueObject';

interface ViewWebArticleProps{
    type: string
    routeProps: RouteComponentProps<any>
}

export default class ViewArticle extends React.Component<ViewWebArticleProps, {}>{


    render(): JSX.Element {
        return (
            <div>
                <Query query={ArticleRequestsValueObject.GET_ARTICLE}
                    variables={{ id: this.props.routeProps.match.params.id }}>
                    {({ loading, error, data }) => {
                        if (error) return <div>Error Article Not Found</div>
                        if (loading) return <Spin size="small" />
                        if (data) {
                            return (
                                this.getArticleView(data.getArticle.article)
                            )
                        }
                    }}
                </Query>
                <Row>
                    <Link to={"/articles/" + this.props.type + "/edit/" + this.props.routeProps.match.params.id}>
                        <Button icon="plus" size="large" type="primary">Edit</Button>
                    </Link>
                </Row>
            </div>
        )
    }

    private getArticleView(article: ArticleModel): JSX.Element {
        return (
            <div>
                <h1>{article.title}</h1>
                <h3>{article.summary}</h3>
                <div dangerouslySetInnerHTML={{ __html: article.body }} />
            </div>

        )
    }
}