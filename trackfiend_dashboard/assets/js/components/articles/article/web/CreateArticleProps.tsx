import ArticleModel from "../../../common/state/ArticlesModel";
import { RouteComponentProps } from "react-router";

export default interface CreateArticleProps{
    onSubmit: (article: ArticleModel) => void,
    routeProps?: RouteComponentProps<any>,
    type: string,
    articleId?: number
}