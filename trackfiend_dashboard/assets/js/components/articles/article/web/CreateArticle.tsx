import * as React from 'react';
import { Form, Input, Button, Select, Spin, Upload, Icon, message, Row, Col } from 'antd';
import ArticleModel from '../../../common/state/ArticlesModel';
import CreateArticleProps from './CreateArticleProps';
import { Mutation, Query } from 'react-apollo';
import { ArticleRequestsValueObject } from '../../../../constants/ArticleRequestsValueObject';
import ErrorModel from '../../../common/error_model';
import { ArtistRequestsValueObject } from '../../../../constants/ArtistRequestsValueObject';
import {ImageUploader} from '../../../common/ImageUploader'
import TextEditor from '../../../common/components/TextEditor';
import { key } from 'resub';
const uuidv1 = require('uuid/v1');

interface CreateArticleState {
    flash?: ErrorModel[],
    artists?: { key: string, label: string }[],
    search?: string,
    articleId?: number,
    articleBody?: string,
    articleTitle?: string,
    articleSummary?: string,
    articleArtists?: number[],
    articleTypeId?: string,
    articleUrl?: string,
    articlePicture?: string
}

export default class CreateArticle<P, S> extends React.Component<CreateArticleProps, CreateArticleState>{
    protected _article: ArticleModel = { artists: [] };
    private _articleType: number;

    constructor(props) {
        super(props);
        this.state = {
            search: "",
            articleBody: null
        };
    }

    onEditorStateChange = (content, editor) => {
        console.log("Article updated", content);
        if(this.state.articleBody == content){
            return;
        }
        this.setState({ articleBody: content })
    }

    private handleArtistsChange = (value: { key: string, label: string }[]) => {
        this._article.artists = [];
        value.map(selection => {
            if (selection.key) {
                this._article.artists.push({id: selection.key, name: selection.label});
            }
        });
        this.setState({ ...this.state, artists: value })
    }

    private fetchArtist = (value) => {
        console.log("Searching for " + value);
        this.setState({ ...{ search: value } })
    }

    private getOptions(artistsArray) {
        if (artistsArray) {
            return (
                artistsArray.map(d =>
                    <Select.Option key={d.id}>
                        {d.name}
                    </Select.Option>)
            )
        }

        else return null
    }

    private getCreateArticleComponent(): JSX.Element {
        return (
            <div className="form-wrapper">
                <Form>
                    <Row>
                        <Col xl={24}>
                            <Form.Item>
                                <label>Article Type</label>
                                <Input defaultValue={this.props.type} disabled={true} />
                            </Form.Item>
                            <Form.Item >
                                <label>Title</label>
                                <Input placeholder="Enter title"
                                    onChange={(e) => {
                                        this.setState({ ...{ articleTitle: e.target.value } })
                                    }}
                                    value ={this.state.articleTitle ? this.state.articleTitle : null}
                                />
                            </Form.Item>
                            <Form.Item>
                                <label>Summary</label>
                                <Input placeholder="Enter Summary here"
                                    onChange={(e) => {
                                        this.setState({ ...{ articleSummary: e.target.value } })
                                    }}
                                    value={this.state.articleSummary ? this.state.articleSummary : null}
                                />
                            </Form.Item>
                            <div style={{ "marginBottom": "10px", "marginLeft": "5px" }}>
                                <label>Content</label>
                            </div>

                            <Form.Item>
                                {this.getBody()}
                            </Form.Item>
                            <Form.Item>
                                <label>Artists</label>
                                <Query query={ArtistRequestsValueObject.SEARCH_ARTIST_BY_NAME}
                                    variables={{ term: this.state.search.replace(/\s/g , "-"), page: 1 }}>
                                    {({ loading, error, data }) => {
                                       // if (error) return <div>Error With Artist Fetch</div>

                                        return (
                                            <Select
                                                mode="multiple"
                                                labelInValue
                                                value={this.state.artists}
                                                placeholder="Enter artist tags here"
                                                notFoundContent={loading ? <Spin size="small" /> : null}
                                                filterOption={false}
                                                onSearch={this.fetchArtist}
                                                onChange={this.handleArtistsChange}
                                                style={{ width: '100%' }}>

                                                {this.getOptions(data.searchArtist)}

                                            </Select>
                                        )
                                    }}
                                </Query>
                            </Form.Item>
                            <Form.Item>
                                <label>Image</label>
                                {this.getImageComponent()}
                            </Form.Item>
                            <Form.Item>
                                <Mutation mutation={this.props.routeProps.match.params.id != null
                                    ? ArticleRequestsValueObject.UPDATE_ARTICLE
                                    : ArticleRequestsValueObject.CREATE_ARTICLE}
                                    onCompleted={(data) => {
                                        if (this.props.routeProps.match.params.id != null) {
                                            if (data.updateArticle.errors) {
                                                this.setState({ flash: data.updateArticle.errors })
                                                return;
                                            }
                                        } else {
                                            if (data.createArticle.errors) {
                                                this.setState({ flash: data.createArticle.errors })
                                                return;
                                            }
                                        }

                                        this.props.routeProps.history.push('/articles/' + this.props.type);
                                    }}
                                    update={(store) => {
                                        // update store value for caching purposes
                                    }}>
                                    {(articleMutation) => (
                                        <Button type="primary" onClick={(e) => {
                                            // Check to see whether logic holds up
                                            let article: ArticleModel = {
                                                id: this.state.articleId,
                                                title: this.state.articleTitle,
                                                body: this.state.articleBody,
                                                artists: this._article.artists.map((value) => {
                                                    if (value) {
                                                        return value
                                                    }
                                                }),
                                                article_type: this.props.type,
                                                summary: this.state.articleSummary,
                                                article_picture: this.state.articlePicture
                                            }
                                            e.preventDefault();
                                            articleMutation({
                                                variables: {
                                                    article: article
                                                }
                                            })
                                        }}>Save</Button>
                                    )}
                                </Mutation>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </div>)
    }

    private getBody = (): JSX.Element =>{
        if (this.state.articleBody) {
            return (
                <TextEditor
                    onEditorChange={ this.onEditorStateChange }
                    initialValue = { this.state.articleBody }
                />
            )
        }

        return (
            <TextEditor
                onEditorChange={ this.onEditorStateChange }
                initialValue = { "this." }
            />
        )
    }

    render(): JSX.Element {
        console.log("Rendering create article component");
        if (this.props.routeProps && this.props.routeProps.match.params.id != null && this.state.articleBody == null) {
            return (
                <Query query={ArticleRequestsValueObject.GET_ARTICLE}
                    fetchPolicy={"cache-and-network"}
                    variables={{ id: this.props.routeProps.match.params.id }}
                    onCompleted={(data) => {
                        if (data) {
                            this.treatData(data.getArticle.article)
                        }
                    }}>
                    {({ loading, error, data }) => {
                        if (error) return <div>Error Article Not Found</div>
                        if (loading) return <Spin size="small" />
                        if (data) {
                          return this.getCreateArticleComponent();
                        }
                    }}
                </Query>
            )
        }
        return (
            this.getCreateArticleComponent()
        )
    }

    private treatData(article) {
        let returnedArtists = article.artists.map((value, index) => {
            if (this._article.artists.indexOf(value.key) == -1) {
                this._article.artists.push({id: value.id, name: value.name});
            }
            return { key: value.id, label: value.name }
        })
        if(this.state.articleBody == article.body
            && this.state.articleTitle == article.title){
                return;
            }

        this.setState({
            ...{
                artists: returnedArtists,
                articleBody: article.body,
                articleSummary: article.summary,
                articleTitle: article.title,
                articleId: article.id,
                articleUrl: article.url,
                articlePicture: article.article_picture
            }
        });
    }

    private getImageComponent() {
        return (
            <div>
                <ImageUploader onImageUpload={ this.onUploadImage } multiple={false}/>
            </div>
        )
    }

    onUploadImage = (info) => {
        const status = info.file.status;
        if (status !== 'uploading') {
            console.log("UPLOADING")
            console.log(info.file, info.fileList);
        }
        if (status === 'done') {
            console.log("UPLOADED SUCCESFULLY")
            message.success(`${info.file.name} file uploaded successfully.`);
        } else if (status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }
    }
}
