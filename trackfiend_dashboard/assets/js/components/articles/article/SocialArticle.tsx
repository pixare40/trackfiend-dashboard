import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import Article from './Article';
import ArticleModel from '../../common/state/ArticlesModel';
import ArticlesStore from '../../../stores/ArticlesStore';
import CreateArticle from './web/CreateArticle';
import ViewSocialArticle from './social/ViewSocialArticle';

export default class SocialArticle extends Article {
    getCreateArticleComponent(): JSX.Element {
        return <CreateArticle routeProps={ this._routeProps } 
                    onSubmit={ this.onSubmit }  type="social"/>
    }

    getEditArticleComponent(): JSX.Element {
        return <CreateArticle routeProps={ this._routeProps } 
                    onSubmit={ this.onSubmit } 
                    type="social"/>
    }
    
    getViewArticleComponent(): JSX.Element {
        return <ViewSocialArticle routeProps={ this._routeProps }/>
    }

    onSubmit = (articleDetails: ArticleModel) =>{
        ArticlesStore.storeArticle(articleDetails);
        let currentProps = this.props as RouteComponentProps<{}>;
        currentProps.history.push("/articles/social");
    }
}