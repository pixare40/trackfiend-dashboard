import * as React from 'react';
import Article from './Article';
import ArticleModel from '../../common/state/ArticlesModel';
import ArticlesStore from '../../../stores/ArticlesStore';
import CreateArticle from './web/CreateArticle';
import ViewArticle from './ViewArticle';

export default class AudioArticle extends Article {
    getCreateArticleComponent(): JSX.Element {
        return <CreateArticle
            routeProps={this._routeProps}
            onSubmit={this.onSubmit} type="audio" />
    }

    getEditArticleComponent(): JSX.Element {
        return (
            <CreateArticle routeProps={this._routeProps}
                onSubmit={this.onSubmit}
                type="audio" />
        )
    }

    getViewArticleComponent(): JSX.Element {
        return (
            <ViewArticle type="audio" routeProps={ this._routeProps }/>
        )
    }
}