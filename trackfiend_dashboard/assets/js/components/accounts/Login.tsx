import * as React from 'react';
import { RouteComponentProps, Link } from 'react-router-dom';
import { Alert, List, Avatar, Row, Col, Button, Form, Input, Upload, Icon, Checkbox } from 'antd';
import { Mutation } from 'react-apollo';
import { LOGIN_MUTATION } from '../../constants/AuthRequestValueObject';
import ArtistModel from '../common/state/artist/ArtistModel';

interface LoginState {
    email?: string,
    password?: string
}

interface LoginProps extends React.Props<any> {
    routeProps: RouteComponentProps<any>
}

export default class CreateAccount extends React.Component<LoginProps, LoginState>{
    //private _account: AccountModel;
    constructor(props: LoginProps) {
        super(props);
    }

    render(): JSX.Element {

        return (
            <div className="form-wrapper">
                <Row>
                    <Col span={6} offset={8}>
                        <Form className="login-form">

                            <Form.Item>

                                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" onChange={
                                    (e) => {
                                        this.setState({ ...{ email: e.target.value } })
                                    }
                                } />

                            </Form.Item>
                            <Form.Item>

                                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password"
                                    onChange={(e) => {
                                        this.setState({ ...{ password: e.target.value } })
                                    }} />

                            </Form.Item>
                            <Form.Item>


                                <Row>
                                    <Checkbox>Remember me</Checkbox>
                                    <a className="login-form-forgot" href="">Forgot password</a>
                                </Row>

                                <Row>
                                    <Mutation mutation={LOGIN_MUTATION} onError={(error)=>{
                                        //At this point handle Error By showing flash
                                        console.log(error);
                                        console.log("Error Login in");
                                    }}>
                                        {(loginMutation) => (
                                            <Button type="primary" htmlType="submit" className="login-form-button" block
                                                onClick={(e) => {
                                                    let login: any = {
                                                        email: this.state.email,
                                                        password: this.state.password
                                                    }
                                                    e.preventDefault();
                                                    loginMutation({
                                                        variables: login
                                                    })
                                                }}>
                                                Log in
                                            </Button>
                                        )}
                                    </Mutation>
                                </Row>

                                <Row>
                                    Or <a href="">register now!</a>
                                </Row>


                            </Form.Item>
                        </Form>
                    </Col>
                </Row>

            </div>
        )
    }

}