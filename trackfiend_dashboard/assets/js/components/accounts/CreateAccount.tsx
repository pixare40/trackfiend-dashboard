import * as React from 'react';
import { RouteComponentProps, Link } from 'react-router-dom';
import { Alert, List, Avatar, Row, Col, Button, Form, Input, Upload, Icon } from 'antd';
import { Mutation } from 'react-apollo';
import { REGISTRATION_MUTATION } from '../../constants/AuthRequestValueObject';
const uuidv1 = require('uuid/v1');

interface CreateAccountState {
    firstname: string,
    lastname: string,
    email: string,
    avatar?: string,
    password: string,
    password_confirmation: string
}

interface CreateAccountProps extends React.Props<any> {
    routeProps: RouteComponentProps<any>
}

export default class CreateAccount extends React.Component<CreateAccountProps, CreateAccountState>{
    //private _account: AccountModel;
    constructor(props: CreateAccountProps) {
        super(props);
    }

    render(): JSX.Element {
        return (
            <div className="form-wrapper">
                <Form>
                    <Row gutter={20} >
                        <Col xl={12} >
                            <label>First Name</label>
                            <Form.Item>
                                <Input onChange={(e) => {
                                    this.setState({ ...{ firstname: e.target.value } })
                                }} placeholder="First Name" />
                            </Form.Item>
                        </Col>
                        <Col xl={12} >
                            <label>Last Name</label>
                            <Form.Item>
                                <Input onChange={(e) => {
                                    this.setState({ ...{ lastname: e.target.value } })
                                }} placeholder="Last Name" />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={20} >
                        <Col xl={12} >
                            <label>Email</label>
                            <Form.Item>
                                <Input onChange={(e) => {
                                    this.setState({ ...{ email: e.target.value } })
                                }} placeholder="Email" />
                            </Form.Item>
                        </Col>
                        <Col xl={12} >
                            <label>Avatar</label>
                            <Form.Item>
                                {
                                    //Upload to cloudinary and get the distinction value
                                }
                                <Upload.Dragger name={uuidv1()} multiple={false}
                                    action={"https://api.cloudinary.com/v1_1/pixare40/image/upload"}>
                                    <p className="ant-upload-drag-icon">
                                        <Icon type="inbox" />
                                    </p>
                                    <p className="ant-upload-text">Click or drag file to this area to upload</p>
                                    <p className="ant-upload-hint">Support for a single or bulk upload. Strictly prohibit from uploading company data or other band files</p>
                                </Upload.Dragger>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={20}>
                        <Col xl={12}>
                            <label>Password</label>
                            <Form.Item>
                                <Input onChange={(e) => {
                                    this.setState({ ...{ password: e.target.value } })
                                }} placeholder="Password" />
                            </Form.Item>
                        </Col>
                        <Col xl={12} >
                            <label>Confirm Password</label>
                            <Form.Item>
                                <Input onChange={(e) => {
                                    this.setState({ ...{ password_confirmation: e.target.value } })
                                }} placeholder="Confirm Password" />
                            </Form.Item>
                        </Col>
                    </Row>

                    <Row gutter={20}>
                        <Col xl={12}>
                            <Form.Item>
                                <Mutation mutation={REGISTRATION_MUTATION} onError={(error) => {
                                    console.log("Error during registration", error);
                                }}>
                                    {
                                        (registrationMutation) => (
                                            <Button type="primary" onClick={(e) => {
                                                let registrationDetails: any = {
                                                    first_name: this.state.firstname,
                                                    last_name: this.state.lastname,
                                                    email: this.state.email,
                                                    password_digest: this.state.password,
                                                    avatar: this.state.avatar
                                                }
                                                e.preventDefault();
                                                registrationMutation({
                                                    variables: {
                                                        input: registrationDetails
                                                    }
                                                })
                                            }}>Create</Button>
                                        )
                                    }
                                </Mutation>
                            </Form.Item>
                        </Col>
                    </Row>

                </Form>
            </div>
        )
    }
}