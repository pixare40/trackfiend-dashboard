import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Button } from 'antd';

export default class Home extends React.Component<{}, {}> {
  constructor(props) {
    super(props)
  }

  public render(): JSX.Element {
    return (
      <div>
        <Button type="primary">Kabaji</Button>
      </div>
    )
  }
}