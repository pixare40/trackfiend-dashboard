import * as React from 'react';
import { RouteComponentProps, Link } from 'react-router-dom';
import { Alert, List, Avatar, Row, Col, Button } from 'antd';
import { ComponentBase } from 'resub';
import ActionsValueObject from '../common/value_objects/ActionsValueObject';
import CreateArtist from './CreateArtist';
import ViewArtists from './ViewArtist';
import { Query } from 'react-apollo';
import { ArtistRequestsValueObject } from '../../constants/ArtistRequestsValueObject';

export default class Artist extends ComponentBase<{}, {}>{

    protected _artistId;
    protected _action: string;

    render(): JSX.Element{
        let currentProps = this.props as RouteComponentProps<any>;
        this._artistId = currentProps.match.params.id;
        this._action = currentProps.match.params.action;
        if(this._action == ActionsValueObject.CREATE){
            return (
                <div>
                    <CreateArtist routeProps={ currentProps } />
                </div>
            )
        }
        else if (this._action == ActionsValueObject.EDIT){
            return(
                <Query query={ ArtistRequestsValueObject.GET_ARTIST_QUERY } variables={ {id:parseInt(this._artistId, 10)} }>
                    {({loading, error, data})=>{
                        if(loading) return <div>Fetching</div>
                        if(error) return <div>Error</div>
                        return (
                            <CreateArtist routeProps={ currentProps } artist={ data.getArtist.artist } />
                        )
                    }}
                </Query>
            )
        }

        return (
            <div>
                <ViewArtists routeProps = { currentProps }/>
            </div>
        )
    }

    getAddNewComponent(): JSX.Element{
        return (
            <Row gutter={16} className="root-row">
                <Col span={4}>
                    <Link to={"/artist/create"}>
                        <Button icon="plus" size="large" type="primary">Add New</Button>
                    </Link>
                </Col>
            </Row>
        )
    }
}