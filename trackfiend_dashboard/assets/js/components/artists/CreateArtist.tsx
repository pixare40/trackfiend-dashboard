import * as React from 'react';
import { RouteComponentProps, Link } from 'react-router-dom';
import { Alert, List, Avatar, Row, Col, Button, Form, Input, DatePicker, Select } from 'antd';
import { Mutation } from 'react-apollo';
import { ArtistRequestsValueObject } from '../../constants/ArtistRequestsValueObject';
import ArtistModel from '../common/state/artist/ArtistModel';
import ErrorModel from '../common/error_model';
import * as moment from 'moment';

interface CreateArtistState {
    artist?: ArtistModel,
    flash?: ErrorModel[]
}

interface CreateArtistProps extends React.Props<any> {
    artist?: ArtistModel,
    routeProps: RouteComponentProps<any>
}

export default class CreateArtist extends React.Component<CreateArtistProps, CreateArtistState>{
    private _artist: ArtistModel;
    private dateFormat = 'YYYY/MM/DD';
    constructor(props: CreateArtistProps) {
        super(props);
        this._artist = {};
        if (this.props.artist) {
            this._artist = {
                id: props.artist.id,
                name: props.artist.name,
                realname: props.artist.realname,
                discogsid: props.artist.discogsid,
                mbid: props.artist.mbid,
                cname: props.artist.cname,
                biography: props.artist.biography,
                artist_picture: props.artist.artist_picture
            }
        }
    }

    render(): JSX.Element {
        return (

            <div className="form-wrapper">
                {this.getFlash()}
                <Form>
                    <Row gutter={20}>
                        <Col xl={12} >
                            <label> Name</label>
                            <Form.Item>
                                <Input placeholder="Name"
                                    onChange={(e) => {
                                        this._artist.name = e.target.value
                                    }}
                                    defaultValue={this.props.artist ? this.props.artist.name : null}
                                />
                            </Form.Item>
                        </Col>
                        <Col xl={12}>
                            <label>Real Name</label>
                            <Form.Item>
                                <Input placeholder="Real Name"
                                    onChange={(e) => {
                                        this._artist.realname = e.target.value
                                    }}
                                    defaultValue={this.props.artist ? this.props.artist.realname : null}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    
                
                    <Row gutter={20}>
                        <Col xl={12} >
                            <label> Discogs ID</label>
                            <Form.Item>
                                <Input placeholder="Discogs ID"
                                    onChange={(e) => {
                                        this._artist.discogsid = e.target.value
                                    }}
                                    defaultValue={this.props.artist ? this.props.artist.discogsid : null}
                                />
                            </Form.Item>
                        </Col>

                        <Col xl={12} >
                            <label>MusicBrainz ID</label>
                            <Form.Item>
                                <Input placeholder="MusicBrainz ID"
                                    onChange={(e) => {
                                        this._artist.mbid = e.target.value
                                    }}
                                    defaultValue={this.props.artist ? this.props.artist.mbid : null}
                                />
                            </Form.Item>
                        </Col>
                    </Row>

                    <Row gutter={20} >
                        <Col xl={12} >
                            <label>Cname</label>
                            <Form.Item>
                                <Input placeholder="Cname"
                                    onChange={(e) => {
                                        this._artist.cname = e.target.value
                                    }}
                                    defaultValue={this.props.artist ? this.props.artist.cname : null}
                                />
                            </Form.Item>
                        </Col>
                        <Col xl={12} >
                            <label>Image URL</label>
                            <Form.Item>
                                <Input placeholder="Image URL"
                                    onChange={(e) => {
                                        this._artist.artist_picture = e.target.value
                                    }}
                                    defaultValue={this.props.artist ? this.props.artist.artist_picture : null}
                                />
                            </Form.Item>
                        </Col>
                    </Row>

                    <Row gutter={20}>
                        <Col xl={12} >
                                <label>Biography</label>
                                <Form.Item>
                                    <Input.TextArea placeholder="Biography"
                                        onChange={(e) => {
                                            this._artist.biography = e.target.value
                                        }}
                                        defaultValue={this.props.artist ? this.props.artist.biography : null}
                                    />
                            </Form.Item>
                        </Col>
                        <Col xl={12}>
                            <label>Comments</label>
                            <Form.Item>
                                <Input.TextArea placeholder="Comments"
                                    onChange={(e)=>{
                                        this._artist.comments = e.target.value
                                    }}
                                    defaultValue={ this.props.artist ? this.props.artist.comments : null } 
                                />
                            </Form.Item>
                        </Col>
                    </Row>

                    <Row>
                        <Col xl={12}>
                            <label>Existence Start</label>
                            <Form.Item>
                                <DatePicker onChange={ (date, dateString)=>{
                                    this._artist.existence_start = dateString;
                                }}
                                />
                            </Form.Item>
                            <label>Existence End</label>
                            <Form.Item>
                                <DatePicker onChange={(date, dateString)=>{
                                    this._artist.existence_end = dateString
                                }} 
                                />
                            </Form.Item>
                        </Col>
                        <Col xl={12}>
                            <label>Area/Country</label>
                            <Form.Item>
                                <Input.TextArea placeholder="Country"
                                    onChange={(e)=>{
                                        this._artist.area = e.target.value;
                                    }}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col xl={12}>
                            <label>Gender</label>
                            <Form.Item>
                                <Select defaultValue="male" onChange={(value)=>{
                                    this._artist.gender = value.toString()
                                }}>
                                    <Select.Option value= "male">
                                        Male
                                    </Select.Option>
                                    <Select.Option value= "female">
                                        Female
                                    </Select.Option>
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col xl={12}>
                            <label>Type</label>
                            <Form.Item>
                                <Select defaultValue="person" onChange={(value)=>{
                                    this._artist.type = value.toString();
                                }}>
                                    <Select.Option value="person">
                                        Person
                                    </Select.Option>
                                    <Select.Option value="group">
                                        Group
                                    </Select.Option>
                                    <Select.Option value="other">
                                        Other
                                    </Select.Option>
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>

                    <Form.Item>
                        <Mutation mutation={this.props.artist 
                            ? ArtistRequestsValueObject.UPDATE_ARTIST_MUTATION 
                            : ArtistRequestsValueObject.ARTIST_MUTATION}
                            onCompleted={(data) => {
                                if (data.createArtist.errors) {
                                    this.setState({flash: data.createArtist.errors})
                                    return;
                                }

                                this.props.routeProps.history.push('/artists/view/');
                            }}
                            update={(store) => {
                                //TODO [ STORE INTEGRATION ] Add fetched components into a store
                            }}>
                            {(artistMutation) => (
                                <Button type="primary" onClick={(e) => {
                                    e.preventDefault();
                                    artistMutation({
                                        variables: {
                                            artist: this._artist
                                        }
                                    });
                                }}>Save</Button>
                            )
                            }
                        </Mutation>

                    </Form.Item>
                </Form>
            </div>
        )
    }

    getFlash = () => {
        if(this.state != null && this.state.flash){
            return (
                <div className="flashContainer">
                        <Alert
                            message="Error"
                            description={this.state.flash[0].key + " " + this.state.flash[0].message}
                            type="error"
                            showIcon
                        />
                </div>
            )
        }
        else{
            <div className="flashContainer">
            </div>
        }
    }
}