import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { ComponentBase } from 'resub';
import { RouteComponentProps } from 'react-router-dom';
import { Query } from 'react-apollo';
import { ArtistRequestsValueObject } from '../../constants/ArtistRequestsValueObject';
import { Card, List } from 'antd';
import ViewAllArtists from './ViewAllArtists';
import { Link } from 'react-router-dom';
import { Image } from 'cloudinary-react';

interface ViewArtistProps{
    routeProps: RouteComponentProps<any>
}

const IconText = ({ icon, text }) => (
    <span>
        {React.createElement(icon, { style: { marginRight: 8 } })}
        {text}
    </span>
);

export default class ViewArtists extends React.Component<ViewArtistProps, {}>{


    render(): JSX.Element {
        let artistId = this.props.routeProps.match.params.id;
        if (artistId == null) {
            return (
                <ViewAllArtists />
            )
        }
        return (
            <div>
                <Query query={ArtistRequestsValueObject.GET_ARTIST_QUERY}
                    variables={{ id: artistId }}>
                    {({ loading, error, data }) => {
                        if (loading) return <div>Fetching</div>
                        if (error) return <div>Error</div>

                        let returnedArtist = data.getArtist.artist;

                        return (
                            <div>
                                <Card title={returnedArtist.name}>
                                    <p>{returnedArtist.realname}</p>
                                </Card>
                                <h1>Articles</h1>
                                
                                <List className="artists-articles" dataSource={returnedArtist.articles} renderItem={item => {
                                    if (item.article_picture) {
                                        return (
                                            <List.Item key={item.id}
                                                actions={[<Link to={"/article/edit/" + item.id}>edit</Link>,
                                                <Link to={"/article/view/" + item.id}>view</Link>]}
                                                extra={
                                                    <Image cloudName="pixare40" publicId={item.article_picture} width="272" crop="scale" />
                                                }>
                                                <List.Item.Meta
                                                    title={<a>{item.title}</a>}
                                                    description={item.summary}
                                                />
                                            </List.Item>
                                        )
                                    }
                                    return (
                                        <List.Item key={item.id}
                                            actions={[<Link to={"/article/edit/" + item.id}>edit</Link>,
                                            <Link to={"/article/view/" + item.id}>view</Link>]}
                                            extra={
                                                <img
                                                    width={272}
                                                    alt="logo"
                                                    src="https://gw.alipayobjects.com/zos/rmsportal/mqaQswcyDLcXyDKnZfES.png"
                                                />
                                            }>
                                            <List.Item.Meta
                                                title={<a>{item.title}</a>}
                                                description={item.summary}
                                            />
                                        </List.Item>
                                    )
                                }}>
                                </List>
                            </div>
                        )
                    }}
                </Query>
            </div>
        )
    }
}