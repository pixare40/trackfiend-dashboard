import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { ComponentBase } from 'resub';
import { ArtistRequestsValueObject } from '../../constants/ArtistRequestsValueObject';
import { Query } from 'react-apollo';
import ArtistModel from '../common/state/artist/ArtistModel';
import { Row, List, Avatar, Pagination } from 'antd';
import { Link } from 'react-router-dom';

interface ViewAllArtistsState{
    currentPage: number
}

export default class ViewAllArtists extends ComponentBase<{}, ViewAllArtistsState>{
    constructor(props){
        super(props)
        this.state = {
            currentPage: 1
        }
    }
    render(): JSX.Element{
        return (
            <div>
                <Query query={ ArtistRequestsValueObject.ARTISTS_QUERY }
                    variables={{ page: this.state.currentPage }}
                    fetchPolicy={"cache-and-network"}>
                        
                            {({ loading, error, data }) => {
                    if (loading) return <div>Fetching</div>
                    if (error) return <div>Error</div>
                    let artists = data.getArtists.artists as ArtistModel[];
                    let totalArtists = data.getArtists.total

                    return(
                        <Row className="root-row">
                            <List itemLayout="horizontal"
                                dataSource={ artists }
                                renderItem={item =>(
                                    <List.Item actions={[<Link to={"/artists/edit/" + item.id}>edit</Link>, 
                                                    <Link to={"/artists/view/" + item.id}>view</Link>]}>
                                        <List.Item.Meta 
                                            avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"/>}
                                            title={<a href={"/artists/view/" + item.id}>{item.name}</a>}
                                        />
                                    </List.Item>
                                )}
                            />
                            <Pagination total={ totalArtists } defaultPageSize={ 50 } 
                                itemRender={
                                    (current, type, originalElement)=>{
                                        if (type === 'prev') {
                                            return <a>Previous</a>;
                                        }
                                        if (type === 'next') {
                                            return <a>Next</a>;
                                        }
                                        return originalElement; 
                                    }
                                } 
                                current={ this.state.currentPage }
                                onChange={ (page, pageSize)=>{
                                    this.setState({currentPage: page});
                                } }/>
                        </Row>
                    )
                }}
                </Query>
            </div>
        )
    }
}