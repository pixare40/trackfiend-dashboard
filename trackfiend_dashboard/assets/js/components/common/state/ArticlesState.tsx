import ArticleModel from './ArticlesModel';

export default interface ArticlesState{
    articleType: string,
    articles: ArticleModel[],
    currentPage: number
}