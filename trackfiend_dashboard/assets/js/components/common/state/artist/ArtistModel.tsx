import moment from 'moment';

export default interface ArtistModel{
    id?: Number,
    name?: string,
    realname?: string,
    discogsid?: string,
    mbid?: string,
    cname?: string,
    biography?: string,
    artist_picture?: string,
    existence_start?: string,
    existence_end?: string,
    area?: string,
    gender?: string,
    comments?: string,
    type?: string
}