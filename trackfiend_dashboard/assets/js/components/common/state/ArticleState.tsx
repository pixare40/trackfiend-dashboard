import ArticleModel from "./ArticlesModel";

export default interface ArticleState{
    currentId: number,
    currentAction: string
}