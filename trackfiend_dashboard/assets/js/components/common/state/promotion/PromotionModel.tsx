import ArtistModel from '../artist/ArtistModel'

export default interface PromotionModel{
    id?: Number,
    promotion_name?: string,
    primary_text?: string,
    secondary_text?: string,
    promotion_image?: string,
    artists?: number[]
}