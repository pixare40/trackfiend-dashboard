export default interface ArticleModel{
    id?: any,
    title?: string,
    body?: string,
    tags?: string,
    article_type?: string,
    url?: string,
    image?: string,
    summary?: string,
    article_picture?: string,
    artists?: {id: string, name: string}[],
    dateStored?: Date,
    dateFetched?: Date
}