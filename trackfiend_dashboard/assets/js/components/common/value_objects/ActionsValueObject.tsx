export default class ActionsValueObject{
    static CREATE: string = "create";
    static EDIT: string = "edit";
    static VIEW: string = "view";
    static UPDATE: string = "update";
}