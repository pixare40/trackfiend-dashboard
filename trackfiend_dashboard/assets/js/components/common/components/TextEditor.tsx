import * as React from 'react';
import { Editor } from '@tinymce/tinymce-react';

interface TextEditorProps extends React.Props<any>{
    onEditorChange: (content, editor) => void,
    initialValue?: string
}

export default class TextEditor extends React.Component<TextEditorProps, {}>{
    render() : JSX.Element{
        return (
            <div>
                <Editor
                    apiKey="3sb5tibwthllwf2z29jk4wpp83bl90pth6zeb0cvxa24zbmy"
                    initialValue= {this.props.initialValue}
                    init={{
                        height: 500,
                        menubar: false,
                        plugins: [
                            'advlist autolink lists link image charmap print preview anchor',
                            'searchreplace visualblocks code fullscreen',
                            'insertdatetime media table paste code help wordcount'
                        ],
                        toolbar:
                            'undo redo | formatselect | bold italic backcolor | \
                alignleft aligncenter alignright alignjustify | \
                bullist numlist outdent indent | removeformat | code'
                    }}
                    onEditorChange={this.props.onEditorChange}
                />
            </div>
        )
    }
}