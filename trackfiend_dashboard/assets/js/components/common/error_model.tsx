export default interface ErrorModel{
    key: string,
    message: string
}