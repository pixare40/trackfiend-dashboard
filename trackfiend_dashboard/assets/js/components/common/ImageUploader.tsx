import * as React from 'react';
import { Upload, Icon, message } from 'antd';
const uuidv1 = require('uuid/v1');

interface ImageUploaderProps{
    onImageUpload: any
    multiple?: boolean
}

export class ImageUploader<P, S> extends React.Component<ImageUploaderProps, {}>{
    render(): JSX.Element{
        return(
            <div>
                <Upload.Dragger name={uuidv1()} onChange={this.props.onImageUpload } 
                multiple={this.props.multiple}
                    action={"https://api.cloudinary.com/v1_1/pixare40/image/upload"}
                    data={this.uploadData}>
                    <p className="ant-upload-drag-icon">
                        <Icon type="inbox" />
                    </p>
                    <p className="ant-upload-text">Click or drag file to this area to upload</p>
                    <p className="ant-upload-hint">Support for a single or bulk upload. Strictly prohibit from uploading company data or other band files</p>
                </Upload.Dragger>
            </div>
        )
    }

    private uploadData = (file) => {
        return (
            {
                file: file,
                upload_preset: "vz4hlywp"
            }
        )
    }

    private onUploadImage = (info) => {
        const status = info.file.status;
        if (status !== 'uploading') {
            console.log("UPLOADING")
            console.log(info.file, info.fileList);
        }
        if (status === 'done') {
            this.setState({ ...{ articlePicture: info.file.response.public_id } })
            console.log("UPLOADED SUCCESFULLY")
            message.success(`${info.file.name} file uploaded successfully.`);
        } else if (status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }
    }
}