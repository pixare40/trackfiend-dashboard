import * as React from 'react';
import { Query } from 'react-apollo';
import { PromotionsRequestValueObject } from '../../constants/PromotionsRequestValueObject';
import { RouteComponentProps } from 'react-router';
import { Image, Transformation } from 'cloudinary-react';
import { Tag, Row, Col, Button } from 'antd';
import { Link } from 'react-router-dom';

interface ViewPromotionsProps extends RouteComponentProps<any> {
}

export class ViewPromotion extends React.Component<ViewPromotionsProps>{
    render(): JSX.Element {
        return (
            <div>
                
                <Row>
                    <Col xl={24}>
                        <Query query={PromotionsRequestValueObject.VIEW_PROMOTION}
                            variables={{ id: this.props.match.params.id }}>
                            {({ loading, error, data }) => {
                                if (error) return <div>Promotion not found</div>
                                if (loading) return <div>Loading promotion</div>
                                if (data) {
                                    return (
                                        <div>
                                            <h1>{data.getPromotion.promotion.promotion_name}</h1>
                                            <h3>{data.getPromotion.promotion.primary_text}</h3>
                                            <h4>{data.getPromotion.promotion.secondary_text}</h4>
                                            <Image cloudName="pixare40" publicId={data.getPromotion.promotion.promotion_image} crop="scale"
                                                width={600} />
                                            <h2>Tagged Artists:</h2>
                                            {this.getArtists(data.getPromotion.promotion.artists)}
                                        </div>
                                    )
                                }
                            }}
                        </Query>
                    </Col>
                </Row>
                <Row>
                    <Link to={ "/promotion/edit/" + this.props.match.params.id }>
                        <Button type="primary">Edit Promotion</Button>
                    </Link>
                </Row>
            </div>
        )
    }

    private getArtists = (artists: any[]) => {
        return (
            <div>
                {artists.map(item => {
                    return (
                        <Tag color='blue'>{item.name}</Tag>
                    )
                })}
            </div>
        )
    }
}