import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Query } from 'react-apollo'
import { PromotionsRequestValueObject } from '../../constants/PromotionsRequestValueObject';
import PromotionModel from '../common/state/promotion/PromotionModel';
import { Row, List } from 'antd';
import { Link } from 'react-router-dom';


export default class ViewAllPromotions extends React.Component{
    render(): JSX.Element{
        return(
            <div>
                <Query query={ PromotionsRequestValueObject.GET_PROMOTIONS }>
                    {({loading, error, data}) =>{
                        if(loading) return <div>Fetching</div>
                        if(error) return <div>Error Fetching Data</div>
                        let promotions = data.getPromotions as PromotionModel[];

                        return(
                            <Row>
                                <List itemLayout="horizontal"
                                    dataSource={ promotions }
                                    renderItem={ item=>(
                                        <List.Item actions={[<Link to={"/"}>edit</Link>,
                                        <Link to={"/"}>view</Link>]}>
                                            <List.Item.Meta 
                                            title={<a href={"/promotions/view/" + item.id}>{item.promotion_name}</a>}/>
                                        </List.Item>
                                    ) }>
                                </List>
                            </Row>
                        )
                    }}
                </Query>
            </div>
        )
    }
}