import * as React from 'react';
import { Editor } from '@tinymce/tinymce-react';
import ErrorModel from '../common/error_model';
import { Form, Row, Col, Input, Select, Spin, Button, Upload, Icon, message } from 'antd';
import { Query, Mutation } from 'react-apollo';
import { ArtistRequestsValueObject } from '../../constants/ArtistRequestsValueObject';
import { PromotionsRequestValueObject } from '../../constants/PromotionsRequestValueObject';
import PromotionModel from '../common/state/promotion/PromotionModel';
import { ImageUploader } from '../common/ImageUploader';
import { RouteComponentProps, Link } from 'react-router-dom';
const uuidv1 = require('uuid/v1');

interface CreatePromotionState {
    id?: number,
    flash?: ErrorModel[],
    promotion_name?: string,
    primary_text?: string,
    secondary_text?: string,
    promotion_image?: string,
    artists?: { key: string, label: string }[],
    searchTerm?: string,
    isFirstRender?: boolean
}

interface CreatePromotionProps extends RouteComponentProps<any> {
}

export default class CreatePromotion extends React.Component<CreatePromotionProps, CreatePromotionState>{

    constructor(props) {
        super(props);
        this.state = {
            isFirstRender: true,
            searchTerm: "a"
        }
    }

    private handleEditorChange = (content, editor) => {
        console.log('Content was updated:', content);
        return ""
    }

    onUploadImage = (info) => {
        const status = info.file.status;
        if (status !== 'uploading') {
            console.log("UPLOADING")
            console.log(info.file, info.fileList);
        }
        if (status === 'done') {
            this.setState({ ...{ promotion_image: info.file.response.public_id } })
            console.log("UPLOADED SUCCESFULLY")
            message.success(`${info.file.name} file uploaded successfully.`);
        } else if (status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }
    }

    private handleArtistsChange = (value: { key: string, label: string }[]) => {
        this.setState({ ...this.state, artists: value })
    }

    private getOptions(artistsArray) {
        if (artistsArray) {
            return (
                artistsArray.map(d =>
                    <Select.Option key={d.id}>
                        {d.name}
                    </Select.Option>)
            )
        }
        else return null
    }

    render() {
        if (this.props.match.params.action) {
            return (
                <Query query={PromotionsRequestValueObject.VIEW_PROMOTION}
                    variables={{ id: this.props.match.params.id }}
                    onCompleted={(data) => {
                        if (data) {
                            this.setData(data.getPromotion.promotion);
                        }
                    }}>
                    {({ loading, error, data }) => {
                        if (error) return <div>Error fetching promotion</div>
                        if (loading) return <Spin size="small" />
                        if (data) {
                            return this.renderContent();
                        }
                    }}
                </Query>
            )

        }
        return (
            this.renderContent()
        );
    }
    private setData(promotion: any) {
        if (!this.state.isFirstRender) {
            return;
        }
        let artistsArray: { key: string, label: string }[] = []
        promotion.artists.map((item, index) => {
            artistsArray.push({ key: item.id, label: item.name })
        })
        this.setState({
            ...{
                id: promotion.id,
                primary_text: promotion.primary_text,
                secondary_text: promotion.secondary_text,
                promotion_name: promotion.promotion_name,
                promotion_image: promotion.promotion_image,
                artists: artistsArray,
                isFirstRender: false
            }
        })
    }

    private renderContent(): JSX.Element {
        return (
            <div>
                <Form>
                    <Row gutter={20}>
                        <Col xl={24}>
                            <Form.Item>
                                <label>Promotion Name</label>
                                <Input placeholder="Promotion Name"
                                    value={this.state.promotion_name}
                                    onChange={(e) => {
                                        this.setState({ ...{ promotion_name: e.target.value } })
                                    }}
                                />
                            </Form.Item>
                            <Form.Item>
                                <label>Primary Text</label>
                                <Input placeholder="Primary Text"
                                    value={this.state.primary_text ? this.state.primary_text : null}
                                    onChange={(e) => {
                                        this.setState({ ...{ primary_text: e.target.value } })
                                    }}
                                />
                            </Form.Item>
                            <Form.Item>
                                <label>Secondary Text</label>
                                <Input placeholder="Secondary Text"
                                    value={this.state.secondary_text ? this.state.secondary_text : null}
                                    onChange={(e) => {
                                        this.setState({ ...{ secondary_text: e.target.value } })
                                    }}
                                />
                            </Form.Item>
                            <Form.Item>
                                <label>Add Promotion Image</label>
                                <ImageUploader onImageUpload={this.onUploadImage} />
                            </Form.Item>
                            <Form.Item>
                                <label>Add Associated Artists</label>
                                <Query query={ArtistRequestsValueObject.SEARCH_ARTIST_BY_NAME}
                                    variables={{ term: this.state.searchTerm }}>
                                    {({ loading, error, data }) => {
                                        if (error) return <div>Error With Artist Fetch</div>
                                        return (
                                            <Select
                                                mode="multiple"
                                                labelInValue
                                                value={this.state.artists}
                                                placeholder="Enter artist tags here"
                                                notFoundContent={loading ? <Spin size="small" /> : null}
                                                filterOption={false}
                                                onChange={this.handleArtistsChange}
                                                style={{ width: '100%' }}>
                                                {this.getOptions(data.searchArtist)}
                                            </Select>
                                        )
                                    }}
                                </Query>
                            </Form.Item>
                            <Form.Item>
                                <Mutation mutation={this.props.match.params.id
                                    ? PromotionsRequestValueObject.UPDATE_PROMOTION
                                    : PromotionsRequestValueObject.CREATE_PROMOTION}
                                    onCompleted={() => {
                                        this.props.match.params.id
                                            ? message.success('Promotion updated succesfully')
                                            : message.success('Promotion created succesfully')
                                    }}>
                                    {(promotionMutation) => (
                                        <Button type="primary" onClick={(e) => {
                                            let artistsArray = []
                                            if (this.state.artists) {
                                                this.state.artists.map(selection => {
                                                    if (selection.key) {
                                                        artistsArray.push(Number.parseInt(selection.key));
                                                    }
                                                });
                                            }

                                            let promotion: PromotionModel = {
                                                id: this.state.id,
                                                promotion_name: this.state.promotion_name,
                                                primary_text: this.state.primary_text,
                                                secondary_text: this.state.secondary_text,
                                                promotion_image: this.state.promotion_image,
                                                artists: artistsArray
                                            }
                                            e.preventDefault();
                                            promotionMutation({
                                                variables: {
                                                    promotion: promotion
                                                }
                                            })
                                        }}>Save</Button>
                                    )}
                                </Mutation>
                                <Link to='/promotions' style={{marginLeft : 8}}><Button type='primary'>Back To Promotions</Button></Link>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
                <Editor
                    apiKey="3sb5tibwthllwf2z29jk4wpp83bl90pth6zeb0cvxa24zbmy"
                    initialValue="<p>This is the initial content of the editor</p>"
                    init={{
                        height: 500,
                        menubar: false,
                        plugins: [
                            'advlist autolink lists link image charmap print preview anchor',
                            'searchreplace visualblocks code fullscreen',
                            'insertdatetime media table paste code help wordcount'
                        ],
                        toolbar:
                            'undo redo | formatselect | bold italic backcolor | \
                alignleft aligncenter alignright alignjustify | \
                bullist numlist outdent indent | removeformat | code'
                    }}
                    onEditorChange={this.handleEditorChange}
                />
            </div>
        )

    }
}