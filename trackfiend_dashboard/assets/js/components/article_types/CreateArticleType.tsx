import * as React from 'react';
import { Form, Input, Button, Select, Spin } from 'antd';
import { Mutation } from 'react-apollo';
import { ArticleRequestsValueObject } from '../../constants/ArticleRequestsValueObject';

export default class CreateArticleType<P, S> extends React.Component<P, S>{
    private _articleType: ArticleType;

    render(): JSX.Element {
        return (
            <div className="form-wrapper">
                <Form>
                    <Form.Item>
                        <Input placeholder="Enter Article Type"
                            onChange={ (e)=>{
                                this._articleType.article_type = e.target.value;
                            } }/>
                    </Form.Item>
                    <Form.Item>
                        <Mutation mutation= {ArticleRequestsValueObject.CREATE_ARTICLE_TYPES}
                            onCompleted={(data)=>{

                            }}>
                            {(articleTypeMutation) => (
                                <Button type="primary" onClick={(e)=>{
                                    e.preventDefault();
                                    articleTypeMutation({
                                        variables:{
                                            articleType: this._articleType
                                        }
                                    })
                                }}>Save</Button>
                            )}
                        </Mutation>
                    </Form.Item>
                </Form>
            </div>
        )
    }
}