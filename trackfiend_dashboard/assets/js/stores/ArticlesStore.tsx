import { StoreBase, AutoSubscribeStore, autoSubscribe, key } from 'resub';
import ArticlesModel from '../components/common/state/ArticlesModel';

@AutoSubscribeStore
class ArticlesStore extends StoreBase{
    
    private _webArticles: ArticlesModel[] = [];
    private _videoArticles: ArticlesModel[] = [];
    private _audioArticles: ArticlesModel[] = [];
    private _socialArticles: ArticlesModel[] = [];

    @autoSubscribe
    public getWebArticles(){
        return this._webArticles;
    }

    @autoSubscribe
    public getVideoArticles(){
        return this._videoArticles;
    }

    @autoSubscribe
    public getAudioArticles(){
        return this._audioArticles;
    }

    @autoSubscribe
    public getSocialArticles(){
        return this._socialArticles;
    }

    @autoSubscribe
    getWebArticle(@key key: any): ArticlesModel {
        return this.getArticle(key, this._webArticles);
    }

    @autoSubscribe
    getSocialArticle(@key key: any): ArticlesModel{
        return this.getArticle(key, this._socialArticles);
    }

    @autoSubscribe
    getVideoArticle(@key key: any): ArticlesModel{
        return this.getArticle(key, this._videoArticles);
    }

    @autoSubscribe
    getAudioArticle(@key key: any): ArticlesModel{
        return this.getArticle(key, this._audioArticles);
    }

    getArticle(key: any, collection: ArticlesModel[]){
        return collection.find((value)=>{
            return value.id == key
        }) || {};
    }

    public storeArticle(article: ArticlesModel){
        // if(article.article_type == "web"){
        //     article.id = this._webArticles.length + 1;
        //     this._webArticles.push(article);
        //     this.trigger();
        // }
    }

    public fetchArticles(){

    }
}

export default new ArticlesStore();