interface ArticleType{
    id: number,
    article_type: string,
    inserted_at: Date,
    updated_at: Date
}