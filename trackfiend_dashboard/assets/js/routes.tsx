import * as React from 'react'
import { Route, Switch, BrowserRouter as Router, Link, Redirect } from 'react-router-dom'

import Root from './Root'
import Home from './components/Home'
import WebArticles from './components/articles/WebArticles';
import SocialArticles from './components/articles/SocialArticles';
import VideoArticles from './components/articles/VideoArticles';
import AudioArticles from './components/articles/AudioArticles';
import WebArticle from './components/articles/article/WebArticle';
import SocialArticle from './components/articles/article/SocialArticle';
import VideoArticle from './components/articles/article/VideoArticle';
import AudioArticle from './components/articles/article/AudioArticle';
import Artist from './components/artists/Artists';
import CreateArticleType from './components/article_types/CreateArticleType';
import CreateAccount from './components/accounts/CreateAccount';
import Login from './components/accounts/Login';
import ViewArticle from './components/articles/article/ViewArticle';
import ViewAllPromotions from './components/promotions/ViewAllPromotions';
import CreatePromotion from './components/promotions/CreatePromotion';
import { ViewPromotion } from './components/promotions/ViewPromotion';



const PrivateRoute = ({component: Component, ...rest}) => (

  sessionStorage.getItem("authVal") == "1"
  ? <Route {...rest} render={(props) =>(
    <Component {...props} />
    )} />
  : <Redirect to='/accounts/login'  />
)


export const routes = (
  <Root>
    <Switch>
      <Route exact path="/" component={ Home } />
      <Route path="/articles/web/:action/:id?" render ={ (props)=>( <WebArticle key={props.match.params.action} {...props}/> ) }/>
      <PrivateRoute exact path="/articles/web" component={  WebArticles }/>
      <Route path="/articles/social/:action/:id?" component={ SocialArticle }/>
      <Route exact path="/articles/social" component={ SocialArticles }/>
      <Route path="/articles/video/:action/:id?" component={ VideoArticle }/>
      <Route exact path="/articles/video" component={ VideoArticles }/>
      <Route path="/articles/audio/:action/:id?" component={ AudioArticle }/>
      <Route exact path="/articles/audio" component={ AudioArticles }/>
      <Route exact path="/artists/:action/:id?" component={ Artist }/>
      <Route exact path="/articleType/create" component={ CreateArticleType }/>
      <Route exact path="/accounts/create" component={ CreateAccount }/>
      <Route exact path="/accounts/login" component={ Login }/>
      <Route exact path="/promotions" component={ ViewAllPromotions } />
      <Route exact path="/promotions/create" component={ CreatePromotion }/>
      <Route exact path="/promotions/view/:id" component={ ViewPromotion } />
      <Route path="/promotion/:action/:id" component={ CreatePromotion } />
    </Switch>
  </Root>
)
