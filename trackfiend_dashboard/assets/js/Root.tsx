import * as React from 'react'
import { Layout, Menu, Breadcrumb, Icon, Avatar } from 'antd';
import { BrowserRouter as Router, Route, Redirect, Link } from 'react-router-dom'
const { SubMenu } = Menu;
const { Header, Content, Sider, Footer } = Layout;
import Login from './components/accounts/Login';

//Login Logic Here
sessionStorage.setItem("authVal", "1");


function SiderFnc() {

  return (
    <Sider width={200} style={{ background: '#fff' }}>
      <Menu
        mode="inline"
        defaultSelectedKeys={['1']}
        defaultOpenKeys={['sub1']}
        style={{ height: '100%', borderRight: 0 }}
      >
        <SubMenu key="sub1" title={<span><Icon type="inbox" />Articles</span>}>
          <Menu.Item key="articles-web"><Link to="/articles/web" >Web Articles</Link></Menu.Item>
          <Menu.Item key="articles-social"><Link to="/articles/social">Social Media Articles</Link></Menu.Item>
          <Menu.Item key="articles-video"><Link to="/articles/video">Video Articles</Link></Menu.Item>
          <Menu.Item key="articles-audio"><Link to="/articles/audio">Audio Articles</Link></Menu.Item>
        </SubMenu>
        <SubMenu key="sub2" title={<span><Icon type="eye" />Promotions</span>}>
          <Menu.Item key="view-all-promotions"><Link to="/promotions">View All Promotions</Link></Menu.Item>
          <Menu.Item key="create-promotion"><Link to="/promotions/create">Create Promotion</Link></Menu.Item>
        </SubMenu>
        <Menu.Item key="sub3"><Icon type="calendar" />Events</Menu.Item>
        <SubMenu key="sub4" title={<span><Icon type="solution" />Artists</span>}>
          <Menu.Item key="view-artists"><Link to="/artists/view">View All Artist</Link></Menu.Item>
          <Menu.Item key="add-artists"><Link to="/artists/create">Add New Artist</Link></Menu.Item>
        </SubMenu>
        <SubMenu key="sub5" title={<span><Icon type="user" />User Management</span>}>
          <Menu.Item key="9"><Link to="/accounts/create">Add New User</Link></Menu.Item>
          <Menu.Item key="10">View All Users</Menu.Item>

        </SubMenu>
        <SubMenu key="sub6" title={<span><Icon type="file" />Pages</span>}>
          <Menu.Item key="9">Add New Page</Menu.Item>
          <Menu.Item key="10">View Pages</Menu.Item>
        </SubMenu>
        <SubMenu key="sub7" title={<span><Icon type="api" />Crawling</span>}>
          <Menu.Item key="9">Initiate Job</Menu.Item>
          <Menu.Item key="10">View Completed</Menu.Item>
          <Menu.Item key="11">Schedule Job</Menu.Item>
          <Menu.Item key="12">View Current Jobs</Menu.Item>
        </SubMenu>
        <SubMenu key="sub8" title={<span><Icon type="dashboard" />Dashboard Administration</span>}>
          <Menu.Item key="9">Add Users</Menu.Item>
          <Menu.Item key="10">View All Users</Menu.Item>
        </SubMenu>
      </Menu>
    </Sider>
  )
}

export default class Root extends React.Component<{}, {}> {
  public render(): JSX.Element {
    return (
      <Layout>
        <Header className="header">
          <div className="logo">
            <Link to="/">Track Fiend Dash</Link>
          </div>
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={['2']}
            style={{ lineHeight: '64px' }}
          >

            <Menu.Item
              key="1"
              style={{ float: 'right' }}
            >
              <Link to="/accounts/login" >
                Log In
        </Link>

            </Menu.Item>

            <Menu.Item
              key="2"
              style={{ float: 'right' }}
            >
              <Link to="/" >
                Log Out
        </Link>
            </Menu.Item>

          </Menu>
        </Header>
        <Layout>

          {

            sessionStorage.getItem("authVal") == "1"
              ? <SiderFnc />
              : ''
          }


          <Layout style={{ padding: '0 24px 24px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Breadcrumb.Item>List</Breadcrumb.Item>
              <Breadcrumb.Item>App</Breadcrumb.Item>
            </Breadcrumb>
            <Content style={{ background: '#fff', padding: 24, margin: 0, minHeight: 280 }}>
              {this.props.children}
            </Content>
          </Layout>

        </Layout>
        <Footer style={{ textAlign: 'center' }}>
          Trackfiend Dashboard {new Date().getFullYear()} Created by Trackfiend
    </Footer>
      </Layout>
    )
  }
}
