import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { routes } from './routes';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
var httpLink = createHttpLink({
    uri: 'http://localhost:4000/api',
});
var cache = new InMemoryCache();
var client = new ApolloClient({
    link: httpLink,
    cache: cache,
});
// This code starts up the React app when it runs in a browser. It sets up the routing
// configuration and injects the app into a DOM element.
ReactDOM.render(React.createElement(BrowserRouter, null,
    React.createElement(ApolloProvider, { client: client }, routes)), document.getElementById('react-app'));
//# sourceMappingURL=app.js.map