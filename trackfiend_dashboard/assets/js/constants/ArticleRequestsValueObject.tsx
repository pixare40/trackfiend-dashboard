import gql from 'graphql-tag';

export const ArticleRequestsValueObject = {
    GET_ARTICLE_TYPES: gql`
    query{
        article_types{
            id
            article_type
        }
    }
    `,

    GET_ARTICLE: gql`
    query GetArticle($id: ID!){
        getArticle(id: $id){
            errors{key message}
            article{
                id
                title
                body
                url
                summary
                article_type
                article_picture
                updated_at
                artists{
                    id
                    name
                    realname
                    cname
                    artistImageUrl
                    bio
                    mbid
                    discogsid
                }
            }
        }
    }
    `,

    CREATE_ARTICLE: gql`
    mutation CreateArticle($article: ArticleInput!){
        createArticle(input: $article){
            errors{key message}
            article{
                id
                title
                body
                url
                summary
                article_type
                artists{
                    name
                }
            }
        }
    }
    `,

    UPDATE_ARTICLE: gql`
    mutation UpdateArticle($article: ArticleInput!){
        updateArticle(input: $article){
            errors{key message}
            article{
                id
                title
                body
                url
                summary
                article_type
                artists{
                    name
                }
            }
        }
    }
    `,

    GET_ARTICLE_TYPE: gql`
    query GetArticleType($type: String!){
        getArticleType(matching: $type){
            id
            article_type
        }
    }
    `,

    CREATE_ARTICLE_TYPES: gql`
    mutation CreateArticleType($article_types: ArticleTypeInput!){
        createArticleType(input: $article_type){
            errors{key message}
            article_type{
                id
                article_type
            }
        }
    }
    `,

    GET_ARTICLES: gql`
    query GetArticles($page: Int!){
        getArticles(page: $page){
            errors{key message}
            count
            articles{
                id
                title
                body
                url
                summary
                artists{
                    name
                }
            }
        }
    }
    `,

    GET_ARTICLE_BY_TYPE: gql`
    query GetArticlesByType($type: String!, $page: Int!){
        getArticlesByType(type: $type, page: $page){
            errors{key message}
            count
            articles{
                id
                title
                body
                url
                summary
                article_type
                article_picture
                artists{ id name }
            }
            
        }
    }
    `
}