import gql from 'graphql-tag';

export const LOGIN_MUTATION = gql`
mutation Login($email: String!, $password: String!){
    login(email: $email, password: $password){
        authToken
        errors{key message}
    }
}
`

export const REGISTRATION_MUTATION = gql`
mutation Register($input: RegistrationInput!){
    register(input: $input){
        account{
            name
            avatar
            email
        }
        errors{key message}
    }
}
`