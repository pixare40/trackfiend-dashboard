import gql from 'graphql-tag';

export const ArtistRequestsValueObject = {
  ARTIST_MUTATION: gql`
    mutation CreateArtist($artist: ArtistInput!) {
        createArtist(input: $artist){
            errors{key message}
            artist {
              id
              name
              realname
              cname
              artist_image_url
              bio
              discogsid
              mbid
            }
        }
    }
  `,

  ARTISTS_QUERY: gql`
    query GetArtists($page: Int!){
      getArtists(page: $page){
        errors{key message}
        total
        artists{
          id
          name
        }
      }
    }
    `,

  GET_ARTIST_QUERY: gql`
    query GetArtist($id: ID!){
      getArtist(id: $id){
        errors{key message}
        artist {
          id
          name
          realname
          cname
          artist_image_url
          bio
          discogsid
          mbid
          articles{
            id
            title
            url
            summary
            article_type
            article_picture
          }
        }
      }
    }`,

  UPDATE_ARTIST_MUTATION: gql`
    mutation UpdateArtist($artist: UpdateArtistInput!){
      updateArtist(input: $artist){
        errors{key message}
        artist {
          id
          name
          realname
          cname
          artist_image_url
          bio
          discogsid
          mbid
        }
      }
    }
    `,

  SEARCH_ARTIST_BY_NAME: gql`
    query SearchArtist($term: String!, $page: Int!){
      searchArtist(term: $term, page: $page){
        id
        name
      }
    }
    `
}