import gql from 'graphql-tag';

export const PromotionsRequestValueObject = {
    GET_PROMOTIONS: gql`
    query{
        getPromotions{
            id
            promotion_name
            primary_text
            secondary_text
            promotion_image
            artists{
                id
                name
            }
        }
    }
    `,

    CREATE_PROMOTION: gql`
    mutation CreatePromotion($promotion: PromotionInput!){
        createPromotion(input: $promotion){
            errors{ key message }
            promotion{
                id
                promotion_name
                primary_text
                secondary_text
                promotion_image
                artists{
                    id
                    name
                }
            }
        }
    }
    `,

    VIEW_PROMOTION: gql`
    query GetPromotion($id: ID!){
        getPromotion(id: $id){
            errors{key message}
            promotion {
                id
                promotion_name
                primary_text
                secondary_text
                promotion_image
                artists{
                    id
                    name
                }
            }
            
        }
    }
    `,

    UPDATE_PROMOTION: gql`
    mutation UpdatePromotion($promotion: PromotionInput!){
        updatePromotion(input: $promotion){
            errors{key message}
            promotion{
                id
                promotion_name
                primary_text
                secondary_text
                promotion_image
                artists{
                    id
                    name
                }
            }
        }
    }
    `
}