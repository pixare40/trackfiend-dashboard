defmodule Trackfiend.Queue.EventQueueConsumer do
  use GenServer
  use AMQP

  alias TrackfiendApi.Artist

  def start_link(_state) do
    GenServer.start_link(__MODULE__, {}, name: __MODULE__)
  end

  @status_queue "status_queue"
  @event_queue "event_queue"
  @queue_error "#{@status_queue}_error"

  def init(_state) do
    {:ok, connection} = Connection.open(username: "rabbitmq", password: "rabbitmq")
    {:ok, channel}  = Channel.open(connection)
    setup_queue(channel)

    :ok = Basic.qos(channel, prefetch_count: 10)
    {:ok, _consumer_tag} = Basic.consume(channel, @status_queue)
    {:ok, channel}
  end

   # Confirmation sent by the broker after registering this process as a consumer
   def handle_info({:basic_consume_ok, %{consumer_tag: _consumer_tag}}, chan) do
    {:noreply, chan}
  end

  # Sent by the broker when the consumer is unexpectedly cancelled (such as after a status_queue deletion)
  def handle_info({:basic_cancel, %{consumer_tag: _consumer_tag}}, chan) do
    {:stop, :normal, chan}
  end

  # Confirmation sent by the broker to the consumer process after a Basic.cancel
  def handle_info({:basic_cancel_ok, %{consumer_tag: _consumer_tag}}, chan) do
    {:noreply, chan}
  end

  def handle_info({:basic_deliver, payload, %{delivery_tag: tag, redelivered: redelivered}}, chan) do
    # You might want to run payload consumption in separate Tasks in production
    consume(chan, tag, redelivered, payload)
    {:noreply, chan}
  end

  defp setup_queue(chan) do
    {:ok, _} = Queue.declare(chan, @queue_error, durable: true)
    # Messages that cannot be delivered to any consumer in the main status_queue will be routed to the error status_queue
    {:ok, _} = Queue.declare(chan, @status_queue,
                             durable: true)
    {:ok, _} = Queue.declare(chan, @event_queue,
                             durable: true)

    # :ok = Exchange.fanout(chan, @exchange, durable: true)
    # :ok = Queue.bind(chan, @status_queue, "")
    # :ok = Queue.bind(chan, @event_queue, "")
  end

  def consume(channel, tag, redelivered, payload) do
    case payload do
      "ready" -> initiate_event_fetch(channel)
      "working" -> report_busy()
      _ -> unhandled_message()
    end
  end

  def initiate_event_fetch(channel) do
    artists = Artist.get_all()
    |> Poison.encode!

    Basic.publish(channel, "", @event_queue, artists)
  end

  def report_busy() do

  end

  def unhandled_message() do

  end
end
