defmodule  Trackfiend.Queue.ArtistUpdateQueueConsumer do
  use GenServer
  use AMQP

#  alias TrackfiendApi.Artist

  def start_link(_state) do
    GenServer.start_link(__MODULE__, {}, name: __MODULE__)
  end

  @artist_update_queue "artist.updates"

  def init(_state) do
    {:ok, connection} = Connection.open(Application.get_env(:trackfiend_api, :rabbit_config))
    {:ok, channel} = Channel.open(connection)
    setup_queue(channel)

    :ok = Basic.qos(channel, prefetch_count: 10)
    {:ok, _consumer_tag} = Basic.consume(channel, @artist_update_queue)
    {:ok, channel}
  end

  def setup_queue(chan) do
    {:ok, _} = Queue.declare(chan, @artist_update_queue, durable: true)
  end

  # Confirmation sent by the broker after registering this process as a consumer
  def handle_info({:basic_consume_ok, %{consumer_tag: _consumer_tag}}, chan) do
    {:noreply, chan}
  end

  # Sent by the broker when the consumer is unexpectedly cancelled (such as after a queue deletion)
  def handle_info({:basic_cancel, %{consumer_tag: _consumer_tag}}, chan) do
    {:stop, :normal, chan}
  end

  # Confirmation sent by the broker to the consumer process after a Basic.cancel
  def handle_info({:basic_cancel_ok, %{consumer_tag: _consumer_tag}}, chan) do
    {:noreply, chan}
  end

  def handle_info({:basic_deliver, payload, %{delivery_tag: tag, redelivered: redelivered}}, chan) do
    # You might want to run payload consumption in separate Tasks in production
    consume(chan, tag, redelivered, payload)
    {:noreply, chan}
  end

  def consume(channel, tag, redelivered, payload) do
    # Absinthe.Subscription.publish(
    #   TrackfiendApiWeb.Endpoint,
    #   payload,
    #   test_subscription: "*")

    Basic.ack(channel, tag)
    artist_service_payload = Poison.decode!(payload)
    artist_name = artist_service_payload["message"]["name"]

    IO.puts "Consumed artist, #{artist_name}."

    rescue
      # Requeue unless it's a redelivered message.
      # This means we will retry consuming a message once in case of exception
      # before we give up and have it moved to the error queue
      #
      # You might also want to catch :exit signal in production code.
      # Make sure you call ack, nack or reject otherwise comsumer will stop
      # receiving messages.
      exception ->
        IO.puts(exception)
        :ok = Basic.reject channel, tag, requeue: not redelivered
        IO.puts "Error consuming payload: #{payload}"
  end

end
