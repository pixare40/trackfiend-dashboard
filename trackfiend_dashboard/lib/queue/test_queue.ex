defmodule Trackfiend.Queue.Test do

  alias Trackfiend.Queue.TestWorker

  def add_item(number) do
    TestWorker.add_number(number)
  end
end
