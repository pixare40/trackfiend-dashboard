defmodule Trackfiend.Queue.TestTask do
  use Task

  alias Trackfiend.Queue.TestWorker

  def start_link(_arg) do
    Task.start_link(&trigger_data_update/0)
  end

  def trigger_data_update() do
    receive do
    after
      5_000 ->
        TestWorker.increment_state()
        trigger_data_update()
    end

  end

end
