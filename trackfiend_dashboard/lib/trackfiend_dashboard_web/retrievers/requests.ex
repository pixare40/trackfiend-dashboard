defmodule Trackfiend.Requests do
  def make_get_request(uri) do
    response = HTTPoison.get(URI.encode(uri), [], hackney: [:insecure])
    IO.inspect(response)
    parse_response(response)
  end

  def make_post_request(uri, payload)
    when is_bitstring(uri) do
      IO.inspect(payload)
      response = HTTPoison.post(URI.encode(uri), Jason.encode!(payload), [{"content-type", "application/json"}], hackney: [:insecure])
      IO.inspect(response)
      parse_response(response)
  end

  def parse_response(response) do
    case response do
      {:ok, %HTTPoison.Response{body: body, status_code: 200}} ->
        {:ok, Jason.decode!(body) }
      {:ok, %HTTPoison.Response{status_code: 404}} ->
        {:error, :not_found }
      {:error, %HTTPoison.Error{reason: reason}} ->
        {:error, reason }
      _ ->
        {:error, "An Unknown Error Occured"}
    end
  end
end
