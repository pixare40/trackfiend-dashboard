defmodule TrackfiendApi.Retriever.ArtistRetriever do
  alias Trackfiend.Requests

  def get_paged_artists(page) do
    artists_endpoint = Application.get_env(:trackfiend_api, :artists_api_endpoint)
    paged_artist_request = artists_endpoint <> "/all/#{page}"
    Requests.make_get_request(paged_artist_request)
  end

  def search_artist(term, page) do
    artists_endpoint = Application.get_env(:trackfiend_api, :artists_api_endpoint)
    search_request = artists_endpoint <> "/Search?searchTerm=#{term}&page=#{page}"
    Requests.make_get_request(search_request)
  end

  def create_artist(artist) do
    create_artist_url = "#{Application.get_env(:trackfiend_api, :artists_api_endpoint)}/create"

    Requests.make_post_request(create_artist_url, artist)
  end

  def update_artist(artist) do
    update = "#{Application.get_env(:trackfiend_api, :artists_api_endpoint)}/update"
    Requests.make_post_request(update, artist)
  end

  def get_artist(id) do
    get = "#{Application.get_env(:trackfiend_api, :artists_api_endpoint)}/GetArtist"
    get_request = get <> "/#{id}"
    Requests.make_get_request(get_request)
  end
end
