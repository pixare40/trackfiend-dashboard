defmodule Trackfiend.Retrievers.ArticlesRetriever do
  alias Trackfiend.Requests

  @articles_endpoint Application.get_env(:trackfiend_api, :articles_api_endpoint)

  def get_article(article_id) do
    article_request = "#{@articles_endpoint}/GetArticle/#{article_id}"
    Requests.make_get_request(article_request)
  end

  def get_articles(page) do
    get_articles_request = "#{@articles_endpoint}/GetArticles/#{page}"
    Requests.make_get_request(get_articles_request)
  end

  def create_article(article) do
    create_article_request = "#{@articles_endpoint}/CreateArticle"
    Requests.make_post_request(create_article_request, article)
  end

  def update_article(article) do
    update_article_request = "#{@articles_endpoint}/UpdateArticle"
    Requests.make_post_request(update_article_request, article)
  end

  def get_by_type(type, page) do
    get_by_type_request = "#{@articles_endpoint}/GetArticlesByType/#{type}/#{page}"
    Requests.make_get_request(get_by_type_request)
  end

end
