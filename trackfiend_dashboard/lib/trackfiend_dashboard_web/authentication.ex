defmodule TrackfiendApi.Authentication do
    def login_with_provider(token, "facebook") do
        attrs = token |> TrackfiendApi.Web.Oauth.Facebook.get_info

        search_params = %{facebook_id: attrs.facebook_id, email: attrs.email}
        TrackfiendApi.Account.get_user_or_create(attrs, search_params)
    end
end
