defmodule TrackfiendApi.Web.Oauth.Facebook do
  @fields "id, email, first_name, last_name"

  def get_info(token) do
    token
    |> get_user
    |> get_profile_picture("small", token)
    |> normalise_user
  end

  defp get_user(token) do
    {:ok, user} = Facebook.me([fields: @fields], token)
    user
  end

  defp get_profile_picture(user, size, token) do
    {:ok, data} = Facebook.picture(user["id"], size, token)
    [user, data]
  end

  defp normalise_user([user, picture_data]) do
    %{
      facebook_id: user["id"],
      avatar: picture_data["data"]["url"],
      firstname: user["first_name"],
      email: user["email"],
      lastname: user["last_name"]
    }
  end
end
