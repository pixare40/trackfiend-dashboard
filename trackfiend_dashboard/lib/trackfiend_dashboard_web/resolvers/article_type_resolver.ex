defmodule TrackfiendApi.Resolvers.ArticleTypeResolver do
  alias TrackfiendApi.Repo
  alias TrackfiendApi.ArticleType

  def all(_args, _info) do
    articleTypes =
      ArticleType
      |> Repo.all
    {:ok, articleTypes}
  end

  def get_by_type(_, %{matching: article_type}, _) do
      {:ok, ArticleType.get_article_type(article_type)}
  end
end
