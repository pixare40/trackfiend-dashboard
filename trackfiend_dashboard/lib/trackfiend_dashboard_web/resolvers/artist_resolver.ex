defmodule TrackfiendApi.Resolvers.ArtistResolver do
  alias TrackfiendApi.Artist
  alias TrackfiendApi.Retriever.ArtistRetriever

  def all(_,%{page: page} ,_) do
    returned = ArtistRetriever.get_paged_artists(page)
    # db_artists = Artist.get_all()
    # {:ok, %{artists: db_artists}}
    case returned do
      {:ok, body} ->
        IO.inspect(body)
        {:ok, body}
      _ ->
        IO.puts("Unknown Error occurred")
        {:error, "Unknown Error"}
    end
  end

  def create(_, %{input: params}, _) do
    IO.puts("Creating artist")
    IO.inspect(params)
    with {:ok, artist} <- ArtistRetriever.create_artist(params) do
      {:ok, %{"artist" => artist}}
    else
      err -> {:error, err}
    end
  end

  def get(_, %{id: id}, _) do
    {:ok, artist} = ArtistRetriever.get_artist(id)
    {:ok, %{"artist" => artist}}
  end

  def update(_, %{input: params}, _) do
    with {:ok, artist} <- Artist.update_artist(params) do
        {:ok, %{"artist" => artist}}
      else
        err -> {:error, err}
      end
  end

  def search(_, %{term: term, page: page}, _) do
    results = ArtistRetriever.search_artist(term, page)
    results
  end

  def get_articles_by_artist(_, %{artist_id: artist_id}, _) do
    detailed_artist = Artist.get_artist_articles(artist_id)
    {:ok, %{"artist" => detailed_artist}}
  end
end
