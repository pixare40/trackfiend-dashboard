defmodule TrackfiendApi.Resolvers.PromotionResolver do
  alias TrackfiendApi.Promotion

  def all(_args, _info) do
    Promotion.get_all()
  end

  def create(_, %{input: params}, _) do
    with {:ok, promotion} <- Promotion.create_promotion(params) do
      {:ok, %{promotion: promotion}}
    else
      err -> err
    end
  end

  @spec get(any, %{id: any}, any) ::
          {:ok, %{promotion: nil | [map] | %{:__struct__ => atom, optional(atom) => any}}}
  def get(_, %{id: id}, _) do
    Promotion.get_promotion(id)
  end

  def update(_, %{input: params}, _) do
    Promotion.update_promotion(params)
  end
end
