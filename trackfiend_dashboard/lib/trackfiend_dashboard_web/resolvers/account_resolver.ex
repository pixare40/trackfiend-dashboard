defmodule TrackfiendApi.Resolvers.AccountResolver do
  alias TrackfiendApi.Repo
  alias TrackfiendApi.Account
  alias TrackfiendApi.{Authentication, Auth}
  alias Trackfiend.Workers.AccountPersistence

  def all(_args, _info) do
    accounts =
      Account
      |> Repo.all

    {:ok, accounts}
  end

  def create(_args, _info) do

  end

  def update(_args, _info) do

  end

  def login_with_provider(_, %{token: token, provider: provider}, _) do
    case provider do
      :facebook ->
        case Authentication.login_with_provider(token, "facebook") do
          {:ok, user} ->
            case Auth.Guardian.encode_and_sign(user) do
              {:ok, auth_token, _} ->
                {:ok, %{token: auth_token}}
              _ ->
                {:error, :error_authenticating}
            end
          _ ->
            {:error, :not_authorised}
        end
    end
  end

  def login(_, %{email: email, password: password}, _) do
    case AccountPersistence.get_by_email(email) do
      {:ok, account} ->
        cond do
          account && Bcrypt.check_pass(account, password, hash_key: :password_digest) ->
            IO.puts "Legit password"
            {:ok, auth_token, _} = Auth.Guardian.encode_and_sign(account)
            IO.inspect(auth_token)
            {:ok, %{"token" => auth_token}}
          account ->
            IO.puts "Bad password"
            {:error, :bad_password}
          end
      {error, error} ->
        {:error, error}
    end
      # true ->
      #   IO.puts "User not found"
      #   {:error, :not_found}
    # end
  end

  def register(_, %{name: name, email: email, password: password}, _) do
    password_digest = Bcrypt.hash_pwd_salt(password)
    case AccountPersistence.create_account(name, email, password_digest) do
      {:ok, result} ->
        IO.inspect(result)
        {:ok, result}
      {:error, message} ->
        IO.puts("User already exists")
        {:error, message}
    end
  end
end
