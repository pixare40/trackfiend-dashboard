defmodule TrackfiendApi.Resolvers.ArticleResolver do
  alias TrackfiendApi.Repo
  alias TrackfiendApi.Article
  alias Trackfiend.Retrievers.ArticlesRetriever

  def create(_, %{input: params}, _) do
    {:ok, result} = ArticlesRetriever.create_article(params)
    {:ok, %{"article" => result}}
  end

  def all(_args, %{page: page},_info) do
    with {:ok, articles} <- ArticlesRetriever.get_articles(page) do
      {:ok, %{"articles" => articles["articles"], "count" => articles["total"]}}
    else
      err -> {:error, err}
    end
  end

  def get(_, %{id: id}, _) do
    with {:ok, result} <- ArticlesRetriever.get_article(id) do
      {:ok, %{"article" => result}}
    else
      err -> {:error, err}
    end
  end

  def update(_, %{input: params}, _) do
    with {:ok, result} <- ArticlesRetriever.update_article(params) do
      {:ok, %{"article" => result}}
    else
      err -> {:error, err}
    end
  end

  def get_by_type(_, %{type: type, page: page}, _) do
    result = ArticlesRetriever.get_by_type(type, page)
    IO.inspect(result)
    with {:ok, article_result} <- ArticlesRetriever.get_by_type(type, page) do
      {:ok, %{"articles" => article_result["articles"], "count" => article_result["total"]}}
    else
      err -> {:error, err}
    end
  end

  def get_pages(_args, _info) do
    with {:ok, articles} <- Article.get_by_type("page", 1) do
    {:ok, articles}
    end
  end

  def get_page(_, %{page: page}, _) do
    article = Article
    |> Repo.get_by(title: page)
    {:ok, %{article: article}}
  end
end
