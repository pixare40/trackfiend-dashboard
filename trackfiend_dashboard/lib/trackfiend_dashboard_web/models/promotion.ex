defmodule TrackfiendApi.Promotion do
  use TrackfiendApiWeb, :model
  alias TrackfiendApi.Promotion
  alias TrackfiendApi.Repo
  alias TrackfiendApi.Artist

  schema "promotions" do
    field :promotion_name, :string
    field :primary_text, :string
    field :secondary_text, :string
    field :promotion_image, :string
    many_to_many(:artists, TrackfiendApi.Artist, join_through: "promotions_artists")

    timestamps()
  end

  def changeset(%Promotion{} = promotion, params) do
    artists = Repo.all(from a in Artist, where: a.id in ^params.artists)

    promotion
    |> cast(params, [:promotion_name, :primary_text, :secondary_text, :promotion_image])
    |> validate_required([:promotion_name, :primary_text, :secondary_text, :promotion_image])
    |> put_assoc(:artists, artists)
  end

  def create_promotion(attrs) do
    %Promotion{}
    |> Promotion.changeset(attrs)
    |> Repo.insert
  end

  def update_promotion(promotion) do
    updatedPromotion = Promotion
    |> Repo.get(promotion.id)
    |> Repo.preload(:artists)
    |> Promotion.changeset(promotion)
    |> Repo.update!
    {:ok, %{promotion: updatedPromotion}}
  end

  def delete_promotion(promotion) do
    fetched_promotion = Repo.get(Promotion, promotion.id)
    case Repo.delete(fetched_promotion) do
      {:ok, result} -> {:ok, result}
      {:error, error} -> {:error, error}
      _ -> {:error, "unknown error occured"}
    end
  end

  def get_all() do
    promotions = Repo.all(Promotion)
    |> Repo.preload(:artists)
    {:ok, promotions}
  end

  def get_promotion(id) do
    promotion =
      Promotion
      |> Repo.get(id)
      |> Repo.preload(:artists)
      {:ok, %{promotion: promotion}}
  end
end
