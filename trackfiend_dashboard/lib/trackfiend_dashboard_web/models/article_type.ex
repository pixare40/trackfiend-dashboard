defmodule TrackfiendApi.ArticleType do
  use TrackfiendApiWeb, :model
  alias TrackfiendApi.ArticleType
  alias TrackfiendApi.Repo

  schema "article_type" do
    field :article_type, :string

    timestamps()
  end

  def get_article_type(article_type) do
    query = from a in ArticleType,
          where: a.article_type == ^article_type,
          select: a
    Repo.one!(query)
  end

  def all(_args, _info) do
    article_type =
    ArticleType
    |> Repo.all
    |> Repo.preload(:artists)
    {:ok, article_type}
  end
end
