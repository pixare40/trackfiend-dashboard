defmodule TrackfiendApi.Account do
  use TrackfiendApiWeb, :model
  alias TrackfiendApi.Account
  alias TrackfiendApi.Repo

  schema "accounts" do
    field :firstname, :string
    field :lastname, :string
    field :email, :string
    field :avatar, :string
    field :facebook_id, :string
    field :password_digest, :string
    field :password, :string, virtual: true
    many_to_many :artists, TrackfiendApi.Artist, join_through: "accounts_artists"

    timestamps()
  end

  def changeset(%Account{} = account, params) do
    account
    |> cast(params, [:firstname, :lastname, :email, :password])
    |> put_password_hash
    |> unique_constraint(:email)
  end

  def put_password_hash(changeset) do
    password = changeset.changes.password
    put_change(changeset, :password_digest, Bcrypt.hash_pwd_salt(password))
  end

  def get_user!(id), do: Repo.get!(Account, id)

  def get_by_email!(email), do: Repo.get_by(Account, email: email)

  def get_user_or_create(attrs, search_params) do
    case Repo.get_by(Account, Map.to_list(search_params)) do
      nil ->
        create_account(attrs)
      user ->
        {:ok, user}
    end
  end

  def create_account(attrs \\ %{}) do
    %Account{}
    |> changeset(attrs)
    |> Repo.insert
  end

  def update_account(%Account{} = user, attrs \\ %{}) do
    user
    |> changeset(attrs)
    |> Repo.update()
  end

  def delete_account(%Account{} = account) do
    Repo.delete(account)
  end

  def list_accounts() do
    Repo.all(Account)
  end

end
