defmodule TrackfiendApi.Artist do
  use TrackfiendApiWeb, :model
  alias TrackfiendApi.Artist
  alias TrackfiendApi.Repo

  @derive {Poison.Encoder, only: [:name, :realname, :cname]}
  schema "artists" do
    field :name, :string
    field :realname, :string
    field :discogsid, :string
    field :mbid, :string
    field :cname, :string
    field :bio, :string
    field :artist_image_url, :string
    many_to_many :accounts, TrackfiendApi.Account, join_through: "accounts_artists"
    many_to_many :articles, TrackfiendApi.Article, join_through: "artists_articles"

    timestamps()
  end

  def changeset(%Artist{} = artist, params) do
    artist
    |> cast(params, [:name, :realname, :discogsid, :mbid, :cname, :bio, :artist_image_url])
    |> validate_required([:name, :cname])
    |> unique_constraint(:cname)
  end

  def create_artist(attrs \\ %{}) do
    %Artist{}
    |> Artist.changeset(attrs)
    |> Repo.insert
  end

  def get_artist(id) do
    artist = Artist
    |> Repo.get!(id)
    |> Repo.preload([articles: [:article_type]])
    {:ok, artist}
  end

  def update_artist(params \\ %{}) do
    IO.puts(params.name)
    artist = get_artist(params.id)
    change = Artist.changeset(artist, params)
    Repo.update(change)
  end

  def search(term) do
    pattern = "%#{term}%"
    Repo.all from q in Artist,
      where: ilike(q.name, ^pattern) or ilike(q.realname, ^pattern)
  end

  @spec get_artist_articles(any) :: any
  def get_artist_articles(id) do
    Artist
    |> Repo.get(id, preload: :articles)
  end

  def get_all() do
      Artist
      |> Repo.all
  end
end
