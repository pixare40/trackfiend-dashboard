defmodule TrackfiendApi.Article do
  use TrackfiendApiWeb, :model
  alias TrackfiendApi.Article
  alias TrackfiendApi.Repo
  alias TrackfiendApi.Artist
  alias TrackfiendApi.ArticleType

  schema "articles" do
    field :title, :string
    field :body, :string
    many_to_many :artists, TrackfiendApi.Artist, join_through: "artists_articles", on_replace: :delete
    belongs_to :article_type, TrackfiendApi.ArticleType
    field :url, :string
    field :summary, :string
    field :article_picture, :string

    timestamps()
  end

  def changeset(%Article{} = article, params) do
    artists = Repo.all(from a in Artist, where: a.id in ^params.artists)

    IO.inspect(artists)
    IO.inspect(params.artists)

    article
    |> Repo.preload(:artists)
    |> cast(params, [:title, :body, :url, :summary, :article_type_id, :article_picture])
    |> foreign_key_constraint(:article_type_id)
    |> put_assoc(:artists, artists)
  end

  def create_article(attrs) do
    result = Mongo.insert_one(:mongo, "trackfiend-articles", attrs)
    IO.inspect(result)
    result
    # %Article{}
    # |> Article.changeset(attrs)
    # |> Repo.insert
  end

  def update_article(article) do
    Article
    |> Repo.get!(article.id)
    |> Article.changeset(article)
    |> Repo.update
  end

  def get_by_type(type, page) do
    article_type = ArticleType.get_article_type(type)

    entries =
      Article
      |> where([a], a.article_type_id == ^article_type.id)
      #|> Repo.preload(:artists)
      |> Repo.paginate(page: page, page_size: 10)

    {:ok, %{entries: entries}}
  end
end
