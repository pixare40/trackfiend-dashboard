defmodule TrackfiendApiWeb.Router do
  use TrackfiendApiWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
  end

  scope "/api" do
    pipe_through :api

    forward "/", Absinthe.Plug,
      schema: TrackfiendApiWeb.Schema,
      interface: :simple,
      socket: TrackfiendApiWeb.UserSocket
  end

  forward "/graphiql", Absinthe.Plug.GraphiQL,
    schema: TrackfiendApiWeb.Schema,
    interface: :simple,
    socket: TrackfiendApiWeb.UserSocket

  scope "/", TrackfiendApiWeb do
    pipe_through :browser # Use the default browser stack

    get "/*path", PageController, :index
  end
end
