defmodule TrackfiendApiWeb.Schema do
  use Absinthe.Schema
  alias TrackfiendApi.Middleware
  alias TrackfiendApi.Resolvers

  import_types __MODULE__.Types
  import_types __MODULE__.AccountTypes

  def middleware(middleware, field, object) do
    new_middleware = {Absinthe.Middleware.MapGet, to_string(field.identifier)}

    middleware
    |> Absinthe.Schema.replace_default(new_middleware, field, object)
    |> apply(:errors, field, object)
  end

  defp apply(middleware, :errors, _field, %{identifier: :mutation}) do
    middleware ++ [Middleware.ChangesetErrors]
  end

  defp apply(middleware, _, _, _) do
    middleware
  end

  query do
    field :get_artists, :artists_result do
      arg :page, non_null(:integer)
      resolve &Resolvers.ArtistResolver.all/3
    end

    field :accounts, list_of(:account) do
      resolve &Resolvers.AccountResolver.all/2
    end

    field :get_artist, :artist_result do
      arg :id, non_null(:id)
      resolve &Resolvers.ArtistResolver.get/3
    end

    field :get_articles, :articles_result do
      arg :page, non_null(:integer)
      resolve &Resolvers.ArticleResolver.all/3
    end

    field :get_articles_by_type, :articles_result do
      arg :type, non_null(:string)
      arg :page, non_null(:integer)
      resolve &Resolvers.ArticleResolver.get_by_type/3
    end

    field :get_article, :article_result do
      arg :id, non_null(:id)
      resolve &Resolvers.ArticleResolver.get/3
    end

    field :search_artist, list_of(:artist) do
      arg :term, :string
      arg :page, non_null(:integer)
      resolve &Resolvers.ArtistResolver.search/3
    end

    field :get_pages, list_of(:article) do
      resolve &Resolvers.ArticleResolver.get_pages/2
    end

    field :get_page, :article_result do
      arg :page, non_null(:string)
      resolve &Resolvers.ArticleResolver.get_page/3
    end

    field :artist_articles, :artist_result do
      arg :artist_id, non_null(:id)
      resolve &Resolvers.ArtistResolver.get_articles_by_artist/3
    end

    field :get_promotions, list_of(:promotion) do
      resolve &Resolvers.PromotionResolver.all/2
    end

    field :get_promotion, :promotion_result do
      arg :id, non_null(:id)
      resolve &Resolvers.PromotionResolver.get/3
    end
  end

  mutation do
    field :create_artist, :artist_result do
      arg :input, non_null(:artist_input)
      resolve &Resolvers.ArtistResolver.create/3
    end

    field :update_artist, :artist_result do
      arg :input, non_null(:update_artist_input)
      resolve &Resolvers.ArtistResolver.update/3
    end

    field :create_account, :account_details do
      resolve &Resolvers.AccountResolver.create/2
    end

    field :update_account, :account_details do
      resolve &Resolvers.AccountResolver.update/2
    end

    field :create_article, :article_result do
      arg :input, non_null(:article_input)
      resolve &Resolvers.ArticleResolver.create/3
    end

    field :update_article, :article_result do
      arg :input, non_null(:article_input)
      resolve &Resolvers.ArticleResolver.update/3
    end

    field :login_with_provider, :login_result do
      arg :token, :string
      arg :provider, type: :provider
      resolve &Resolvers.AccountResolver.login_with_provider/3
    end

    field :login, :login_result do
      arg :email, non_null(:string)
      arg :password, non_null(:string)
      resolve &Resolvers.AccountResolver.login/3
    end

    field :register, :registration_result do
      arg :name, non_null(:string)
      arg :email, non_null(:string)
      arg :password, non_null(:string)
      resolve &Resolvers.AccountResolver.register/3
    end

    field :create_promotion, :promotion_result do
      arg :input, non_null(:promotion_input)
      resolve &Resolvers.PromotionResolver.create/3
    end

    field :update_promotion, :promotion_result do
      arg :input, non_null(:promotion_input)
      resolve &Resolvers.PromotionResolver.update/3
    end
  end

  subscription do
    field :test_subscription, :id do
      config fn _args, _info ->
        {:ok, topic: "*"}
      end
    end
  end

  object :input_error do
    field :key, non_null(:string)
    field :message, non_null(:string)
  end
end
