defmodule TrackfiendApiWeb.GuardianErrorView  do
    use TrackfiendApiWeb, :view
    def render("forbidden.json", _assigns) do
        %{message: "Forbidden"}
    end
end
