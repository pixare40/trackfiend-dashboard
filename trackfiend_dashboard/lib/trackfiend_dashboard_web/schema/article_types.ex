defmodule TrackfiendApi.Schema.ArticleTypes do
  use Absinthe.Schema.Notation

  object :article do
    field :id, :id
    field :article_type, :string
  end
end
