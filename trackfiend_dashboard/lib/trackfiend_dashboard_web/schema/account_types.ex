defmodule TrackfiendApiWeb.Schema.AccountTypes do
  use Absinthe.Schema.Notation

  object :account do
    field :id, :id
    field :firstname, :string
    field :lastname, :string
    field :email, :string
    field :artists, list_of(:artist)
    field :token, :string
  end

  #More enum values can be provided later including email login and gmail login
  enum :provider do
    value :facebook
  end
end
