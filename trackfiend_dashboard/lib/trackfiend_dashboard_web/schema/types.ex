defmodule TrackfiendApiWeb.Schema.Types do
  use Absinthe.Schema.Notation

  import_types Absinthe.Type.Custom

  #import Absinthe.Resolution.Helpers

  object :artist do
    field :id, :id
    field :name, :string
    field :realname, :string
    field :discogsid, :string
    field :mbid, :string
    field :cname, :string
    field :bio, :string
    field :artist_image_url, :string
    field :accounts, list_of(:account)
    field :articles, list_of(:article)
  end

  object :article do
    field :id, :id
    field :title, :string
    field :body, :string
    field :url, :string
    field :summary, :string
    field :article_type, :string
    field :updated_at, :date
    field :artists, list_of(:artist)
    field :article_picture, :string
  end

  object :account_details do
    field :id, :id
    field :name, :string
    field :email, :string
    field :avatar, :string
  end

  object :promotion do
    field :id, :id
    field :promotion_name, :string
    field :primary_text, :string
    field :secondary_text, :string
    field :promotion_image, :string
    field :artists, list_of(:artist)
  end

  object :article_type do
    field :id, :id
    field :article_type, :string
  end

  object :artist_result do
    field :artist, :artist
    field :errors, list_of(:input_error)
  end

  object :artists_result do
    field :artists, list_of(:artist)
    field :total, :integer
    field :errors, list_of(:input_error)
  end

  object :articles_result do
    field :articles, list_of(:article)
    field :count, :integer
    field :errors, list_of(:input_error)
  end

  object :article_result do
    field :article, :article
    field :errors, list_of(:input_error)
  end

  object :promotion_result do
    field :promotion, :promotion
    field :errors, list_of(:input_error)
  end

  object :login_result do
    field :token, :string
    field :errors, list_of(:input_error)
  end

  object :registration_result do
    field :account, :account_details
    field :errors, list_of(:input_error)
  end

  input_object :article_artist_input do
    field :id, :id
    field :name, :string
  end

  input_object :artist_input do
    field :name, non_null(:string)
    field :realname, :string
    field :discogsid, :string
    field :mbid, :string
    field :cname, non_null(:string)
    field :existence_start, :date
    field :existence_end, :date
    field :area, :string
    field :gender, :string
    field :biography, :string
    field :comments, :string
    field :type, :string
    field :artist_picture, :string
  end

  input_object :update_artist_input do
    field :id, non_null(:id)
    field :name, non_null(:string)
    field :realname, :string
    field :discogsid, :string
    field :mbid, :string
    field :cname, non_null(:string)
    field :bio, :string
    field :artist_picture, :string
  end

  input_object :article_input do
    field :id, :id
    field :title, non_null(:string)
    field :body, non_null(:string)
    field :url, :string
    field :summary, :string
    field :article_picture, :string
    field :article_type, :string
    field :artists, list_of(:article_artist_input)
  end

  input_object :article_type_search_input do
    field :article_type, non_null(:string)
  end

  input_object :get_artist_input do
    field :id, non_null(:integer)
  end

  input_object :promotion_input do
    field :id, :id
    field :promotion_name, non_null(:string)
    field :primary_text, non_null(:string)
    field :secondary_text, non_null(:string)
    field :promotion_image, non_null(:string)
    field :artists, list_of(:id)
  end
end
