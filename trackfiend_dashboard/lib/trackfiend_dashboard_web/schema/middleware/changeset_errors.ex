defmodule TrackfiendApi.Middleware.ChangesetErrors do
  @behaviour Absinthe.Middleware

  def call(res, _) do
    with %{errors: [%Ecto.Changeset{} = changeset]} <- res do
      %{res |
        value: %{errors: transform_errors(changeset)},
        errors: [],
      }
    end
  end

  defp transform_errors(changeset) do
    IO.inspect(changeset)
    error_map =
      changeset
    |> Ecto.Changeset.traverse_errors(&format_error/1)
    IO.inspect(error_map)
    Enum.map(error_map, fn {key, value} ->
        %{key: key, message: value}
    end)
  end

  defp format_error({msg, opts}) do
    Enum.reduce(opts, msg, fn {key, value}, acc ->
      String.replace(acc, "%{#{key}}", to_string(value))
    end)
  end
end
