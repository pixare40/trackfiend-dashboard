defmodule TrackfiendApi.Schema.ArtistTypes do
  use Absinthe.Schema.Notation

  object :artist do
    field :id, :id
    field :name, :string
    field :realname, :string
    field :discogsid, :string
    field :mbid, :string
    field :cname, :string
    field :bio, :string
    field :artist_image_url, :string
    field :accounts, list_of(:account)
    field :articles, list_of(:article)
  end
end
