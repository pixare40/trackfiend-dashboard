defmodule TrackfiendApiWeb.GuardianErrorHandler do
    use TrackfiendApiWeb, :controller
    alias TrackfiendApiWeb.GuardianErrorView

    def unauthenticated(conn, _params) do
      conn
      |> put_status(:forbidden)
      |> render(GuardianErrorView, :forbidden)
    end
end
