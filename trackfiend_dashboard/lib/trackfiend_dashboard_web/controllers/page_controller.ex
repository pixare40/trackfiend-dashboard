defmodule TrackfiendApiWeb.PageController do
  use TrackfiendApiWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
