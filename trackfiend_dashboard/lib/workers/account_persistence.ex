defmodule Trackfiend.Workers.AccountPersistence do
  use GenServer

  def start_link(_state) do
    GenServer.start_link(__MODULE__, {}, name: __MODULE__)
  end

  def init(state) do
    {:ok, state}
  end

  def handle_call({:create_account, name, email, password_digest}, _from,state) do
    case Mongo.find_one(:mongo, "trackfiend-accounts", %{email: email}) do
      nil ->
        {:ok, result} = Mongo.insert_one(:mongo,
          "trackfiend-accounts", %{id: Ecto.UUID.generate(),
          name: name, email: email, password_digest: password_digest})

        {:reply, {:ok, result}, state}
      _document ->
        {:reply, {:error, "Existent User"}, state}

    end
  end

  def handle_call({:get_by_email, email}, _from, state) do
    case Mongo.find_one(:mongo, "trackfiend-accounts", %{email: String.trim(email)}) do
      nil ->
        {:reply, {:error, "User Does Not Exist"}, state}
      account ->
        IO.inspect(account)
        {:reply, {:ok, account}, state}
    end
  end

  def create_account(name, email, password_digest) do
    GenServer.call(__MODULE__, {:create_account, name, email, password_digest})
  end

  def get_by_email(email) do
    GenServer.call(__MODULE__, {:get_by_email, email})
  end
end
