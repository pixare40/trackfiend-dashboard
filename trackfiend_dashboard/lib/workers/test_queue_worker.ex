defmodule Trackfiend.Queue.TestWorker do
  use GenServer

  def start_link(_state) do
    GenServer.start_link(__MODULE__, 0, name: __MODULE__)
  end

  def add_number(number) do
    GenServer.cast(__MODULE__, {:add_number, number})
  end

  def increment_state() do
    GenServer.cast(__MODULE__, {:increment_number})
  end

  def init(state) do
    {:ok, state}
  end

  def handle_cast({:add_number, number}, state) do
    {:noreply, state + number}
  end

  def handle_cast({:increment_number}, state) do
    new_state = state + 1
    Absinthe.Subscription.publish(
      TrackfiendApiWeb.Endpoint,
      new_state,
      test_subscription: "*")
    {:noreply, new_state}
  end
end
