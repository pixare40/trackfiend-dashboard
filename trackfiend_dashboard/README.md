# TrackfiendApi

To start your Trackfiend API app:

  * Install dependencies with `mix deps.get`
  * Run C++ build tool For Windows Systems
  ```
  cmd /K "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64
  ```
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

# Creating Developement Environment
Create the "trackfiend_api_dev" db first. 

Install the dev_env.backup file in your postgres install by running the following command

```
 docker exec -i 8648de786624 psql -U postgres trackfiend_api_dev < dev_env.backup
```
Build application
```
docker build -t trackfiend_api .
```

# Configuring the Solr Search Instance
The Solr instance has to be started and the core created to enable search functionality
```
docker run --name solr -d -p 8983:8983 --network="eventservice_trackfiend" -t solr:8.5
```
We have to start the Solr instance in the **eventservice_trackfiend** network as all Trackfiend applications are currenctly running on this docker network.

If the **artist**, **events** and **articles** cores are not available you have to recreate them

### Artists
```
docker exec -it --user=solr solr bin/solr create_core -c artists
```

### Events
```
docker exec -it --user=solr solr bin/solr create_core -c events
```

### Articles
```
docker exec -it --user=solr solr bin/solr create_core -c articles
```

## Deploy and Run

Deploy and start RabbitMQ and Postgres
```
docker-compose -f docker-compose.yml up
```
Then deploy and start Trackfiend
```
docker-compose -f stack.yml up
```