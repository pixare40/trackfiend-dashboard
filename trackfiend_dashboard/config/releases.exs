import Config

# db_host = System.get_env("DATABASE_HOST") ||
#   raise """
#   environment variable DATABASE_HOST is missing.
#   """
# db_database = System.get_env("DATABASE_DB") || "my_app_dev"
# db_username = System.get_env("DATABASE_USER") || "postgres"
# db_password = System.get_env("DATABASE_PASSWORD") || "postgres"
# db_url = "ecto://#{db_username}:#{db_password}@#{db_host}/#{db_database}"

# secret_key_base = System.get_env("SECRET_KEY_BASE") ||
#   raise """
#   environment variable SECRET_KEY_BASE is missing.
#   You can generate one by calling: mix phx.gen.secret
#   """

config :trackfiend_api, TrackfiendApiWeb.Endpoint,
  http: [:inet6, port: 4000],
  secret_key_base: "hxt19K6c+Lyl2E0FQ5nd/IvYh1xHl/Zyko3uwscpUT3C02p8WqEjyRBdYlwnlh98"
