use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :trackfiend_api, TrackfiendApiWeb.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [
    {"node", [
      "node_modules/webpack/bin/webpack.js",
      "--watch-stdin",
      "--colors",
      cd: Path.expand("../assets", __DIR__)
    ]}
  ]


# Watch static and templates for browser reloading.
config :trackfiend_api, TrackfiendApiWeb.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{lib/trackfiend_dashboard_web/views/.*(ex)$},
      ~r{lib/trackfiend_dashboard_web/templates/.*(eex)$}
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :trackfiend_api, TrackfiendApi.Repo,
  username: "postgres",
  password: "admin",
  database: "trackfiend_api_dev",
  hostname: "localhost",
  port: 5432,
  pool_size: 10

#facebook dev configuration
config :facebook,
  app_id: 1693945244243962 ,
  app_secret: nil,
  app_access_token: "EAAYEolyNVZCoBAPW6QZA7A1EbQbJ9AGtLrjVxc9nOgRhMqoSRYuc8xnsC6MpNFpbr7YG3OfsZC87iXwdSZBD6tNFZCaWe0HUwA15ZBoaH7BAS0ZBgNTu63b3cjea2eixemQDMP8da1i219DuzMOmLwha9RXgXZCdAzBgTSA1OzVUuZBfZBew4ZAVvSQLVsSKPVIRg4ZD",
  graph_url: "https://graph.facebook.com",
  graph_video_url: "https://graph-video.facebook.com"

# RabbitMQ AMQP Uri
rabbit_config = "amqp://rabbitmq:rabbitmq@localhost:5672"

config :trackfiend_api,
  rabbit_config: rabbit_config,
  artists_api_endpoint: "https://localhost:34000/artist",
  articles_api_endpoint: "https://localhost:36000/api/articles",
  mongo_endpoint: "mongodb://localhost:27017/trackfiend-articles"
