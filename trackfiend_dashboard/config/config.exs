# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :trackfiend_api,
  ecto_repos: [TrackfiendApi.Repo]

# Configures the endpoint
config :trackfiend_api, TrackfiendApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "33a8Tpygua8Zvg30ypzMN2pL64gVIeUG9eKmzU+x01m4Ndxl6LrlQ3QE1R6j3WX3",
  render_errors: [view: TrackfiendApiWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: TrackfiendApi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

#Configure Guardian
config :trackfiend_api, TrackfiendApi.Auth.Guardian,
  issuer: "trackfiend_api",
  ttl: { 30, :days },
  verify_issuer: true,
  secret_key: "xvIIfGrbuOG7W1lbXee5uSevJflAFStpRxkPKbnpfXegpTBVTStwk3hPzgAC1JkN",
  serializer: TrackfiendApi.Auth.Guardian
