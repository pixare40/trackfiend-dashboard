-- Adminer 4.7.6 PostgreSQL dump

\connect "trackfiend_api_dev";

DROP TABLE IF EXISTS "accounts";
DROP SEQUENCE IF EXISTS accounts_id_seq;
CREATE SEQUENCE accounts_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."accounts" (
    "id" bigint DEFAULT nextval('accounts_id_seq') NOT NULL,
    "firstname" character varying(255),
    "lastname" character varying(255),
    "email" character varying(255),
    "password_digest" character varying(255),
    "inserted_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL,
    "facebook_id" character varying(255),
    "avatar" character varying(255),
    CONSTRAINT "accounts_email_index" UNIQUE ("email"),
    CONSTRAINT "accounts_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "accounts" ("id", "firstname", "lastname", "email", "password_digest", "inserted_at", "updated_at", "facebook_id", "avatar") VALUES
(1,	'Kabaji',	'Egara',	'kabajiegara@live.com',	NULL,	'2018-07-09 15:01:02.48',	'2018-07-09 15:01:02.485',	NULL,	NULL);

DROP TABLE IF EXISTS "accounts_artists";
DROP SEQUENCE IF EXISTS accounts_artists_id_seq;
CREATE SEQUENCE accounts_artists_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."accounts_artists" (
    "id" bigint DEFAULT nextval('accounts_artists_id_seq') NOT NULL,
    "account_id" bigint,
    "artist_id" bigint,
    "inserted_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL,
    CONSTRAINT "accounts_artists_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "accounts_artists_account_id_fkey" FOREIGN KEY (account_id) REFERENCES accounts(id) NOT DEFERRABLE,
    CONSTRAINT "accounts_artists_artist_id_fkey" FOREIGN KEY (artist_id) REFERENCES artists(id) NOT DEFERRABLE
) WITH (oids = false);


DROP TABLE IF EXISTS "article_type";
DROP SEQUENCE IF EXISTS article_type_id_seq;
CREATE SEQUENCE article_type_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."article_type" (
    "id" bigint DEFAULT nextval('article_type_id_seq') NOT NULL,
    "article_type" character varying(255),
    "inserted_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL,
    CONSTRAINT "article_type_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "article_type" ("id", "article_type", "inserted_at", "updated_at") VALUES
(5,	'web',	'2018-09-24 21:11:30.813939',	'2018-09-24 21:11:30.813939'),
(7,	'audio',	'2018-09-24 21:11:55.315025',	'2018-09-24 21:11:55.315025'),
(8,	'video',	'2018-09-24 21:12:19.147494',	'2018-09-24 21:12:19.147494'),
(9,	'social',	'2018-09-24 21:12:46.85021',	'2018-09-24 21:12:46.85021');

DROP TABLE IF EXISTS "articles";
DROP SEQUENCE IF EXISTS articles_id_seq;
CREATE SEQUENCE articles_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."articles" (
    "id" bigint DEFAULT nextval('articles_id_seq') NOT NULL,
    "title" character varying(255),
    "body" character varying(255),
    "url" character varying(255),
    "summary" character varying(255),
    "article_type_id" bigint,
    "inserted_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL,
    "article_picture" character varying(255),
    CONSTRAINT "articles_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "articles_article_type_fkey" FOREIGN KEY (article_type_id) REFERENCES article_type(id) NOT DEFERRABLE
) WITH (oids = false);

INSERT INTO "articles" ("id", "title", "body", "url", "summary", "article_type_id", "inserted_at", "updated_at", "article_picture") VALUES
(10,	'rtut5ryu',	'<p>tyriujtyui</p>',	NULL,	'yurtu',	NULL,	'2018-10-09 19:41:50.684',	'2018-10-09 19:41:50.684',	NULL),
(11,	'RTYFYTGJ',	'<p>HJGHJ</p>',	NULL,	'GHJ',	5,	'2018-10-09 20:19:41.24',	'2018-10-09 20:19:41.24',	NULL),
(13,	'tryut',	'<p>ytutyu</p>',	NULL,	'tyutyu',	5,	'2018-10-09 20:41:25.311',	'2018-10-09 20:41:25.311',	NULL),
(14,	'dfghf',	'<p>dfhgfd</p>',	NULL,	'dfgh',	5,	'2018-10-09 20:50:50.386',	'2018-10-09 20:50:50.386',	NULL),
(15,	'ert',	'<p>yhrtur6ty</p>',	NULL,	'yrt',	5,	'2018-10-09 20:57:11.783',	'2018-10-09 20:57:11.787',	NULL),
(16,	'special',	'<p>special</p>',	NULL,	'etrh',	5,	'2018-10-09 21:04:25.102',	'2018-10-09 21:04:25.111',	NULL),
(17,	'ertyrety',	'<p>rtyrt</p>',	NULL,	'iluo',	5,	'2018-10-09 21:11:44.341',	'2018-10-09 21:11:44.356',	NULL),
(12,	'rtyuh',	'<p>rtyhrt</p>
<p>Kabaji</p>
<p>blahhh</p>',	NULL,	'rt',	5,	'2018-10-09 20:33:00.353',	'2019-01-28 20:24:43.933',	NULL),
(18,	'With picture',	'<p>With picture</p>',	NULL,	'With picture',	5,	'2019-03-20 21:10:32.728',	'2019-03-20 21:10:32.748',	NULL),
(19,	'Kabaji Image Test',	'<p>Test of the imaging testing</p>',	NULL,	'Kabaji Image Test',	5,	'2020-03-25 19:26:31.606473',	'2020-03-25 19:26:31.610268',	NULL),
(20,	'test 2',	'<p>second test for pictures</p>',	NULL,	'test 2',	5,	'2020-03-25 19:38:29.849399',	'2020-03-25 19:38:29.853972',	'z64ychh8tgpyccjnhzqo');

DROP TABLE IF EXISTS "artists";
DROP SEQUENCE IF EXISTS artists_id_seq;
CREATE SEQUENCE artists_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."artists" (
    "id" bigint DEFAULT nextval('artists_id_seq') NOT NULL,
    "name" character varying(255),
    "realname" character varying(255),
    "discogsid" character varying(255),
    "mbid" character varying(255),
    "cname" character varying(255) NOT NULL,
    "bio" character varying(255),
    "socials" character varying(255),
    "artist_image_url" character varying(255),
    "inserted_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL,
    CONSTRAINT "artists_cname_index" UNIQUE ("cname"),
    CONSTRAINT "artists_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "artists" ("id", "name", "realname", "discogsid", "mbid", "cname", "bio", "socials", "artist_image_url", "inserted_at", "updated_at") VALUES
(1,	'Future',	'Nayvadius DeMun Wilburn',	NULL,	NULL,	'future',	'a baller',	NULL,	NULL,	'2018-07-23 21:28:51.609',	'2018-08-02 19:40:17.071'),
(31,	'dfd',	'dfd',	NULL,	NULL,	'df',	NULL,	NULL,	NULL,	'2018-08-06 21:24:49.572',	'2018-08-06 21:24:49.576'),
(42,	'gtyhtgu',	NULL,	NULL,	NULL,	'dfd',	NULL,	NULL,	NULL,	'2018-08-27 16:17:51.907',	'2018-08-27 16:17:51.956'),
(45,	'KabajiE',	'Kabaji Egara',	NULL,	NULL,	'kabajie',	'Crazy Muthafer',	NULL,	NULL,	'2019-01-28 19:56:05.634',	'2019-01-28 19:56:05.634');

DROP TABLE IF EXISTS "artists_articles";
DROP SEQUENCE IF EXISTS artists_articles_id_seq;
CREATE SEQUENCE artists_articles_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."artists_articles" (
    "id" bigint DEFAULT nextval('artists_articles_id_seq') NOT NULL,
    "artist_id" bigint,
    "article_id" bigint,
    "inserted_at" timestamp DEFAULT now() NOT NULL,
    "updated_at" timestamp DEFAULT now() NOT NULL,
    CONSTRAINT "artists_articles_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "artists_articles_article_id_fkey" FOREIGN KEY (article_id) REFERENCES articles(id) ON DELETE CASCADE NOT DEFERRABLE,
    CONSTRAINT "artists_articles_artist_id_fkey" FOREIGN KEY (artist_id) REFERENCES artists(id) ON DELETE CASCADE NOT DEFERRABLE
) WITH (oids = false);

INSERT INTO "artists_articles" ("id", "artist_id", "article_id", "inserted_at", "updated_at") VALUES
(6,	1,	10,	'2018-10-09 20:41:50.582554',	'2018-10-09 20:41:50.582554'),
(7,	1,	11,	'2018-10-09 21:19:41.229645',	'2018-10-09 21:19:41.229645'),
(9,	1,	13,	'2018-10-09 21:41:25.306156',	'2018-10-09 21:41:25.306156'),
(10,	1,	14,	'2018-10-09 21:50:50.374529',	'2018-10-09 21:50:50.374529'),
(11,	1,	15,	'2018-10-09 21:57:11.767498',	'2018-10-09 21:57:11.767498'),
(12,	1,	16,	'2018-10-09 22:04:25.084937',	'2018-10-09 22:04:25.084937'),
(13,	1,	17,	'2018-10-09 22:11:44.324956',	'2018-10-09 22:11:44.324956'),
(17,	31,	13,	'2018-12-04 21:02:03.867256',	'2018-12-04 21:02:03.867256'),
(18,	1,	18,	'2019-03-20 21:10:32.66072',	'2019-03-20 21:10:32.66072'),
(19,	1,	19,	'2020-03-25 19:26:31.571916',	'2020-03-25 19:26:31.571916'),
(20,	1,	20,	'2020-03-25 19:38:29.841986',	'2020-03-25 19:38:29.841986');

DROP TABLE IF EXISTS "promotions";
DROP SEQUENCE IF EXISTS promotions_id_seq;
CREATE SEQUENCE promotions_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."promotions" (
    "id" bigint DEFAULT nextval('promotions_id_seq') NOT NULL,
    "promotion_name" character varying(255),
    "primary_text" character varying(255),
    "secondary_text" character varying(255),
    "promotion_image" character varying(255),
    "artists" bigint,
    "inserted_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL,
    CONSTRAINT "promotions_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "promotions_artists_fkey" FOREIGN KEY (artists) REFERENCES artists(id) NOT DEFERRABLE
) WITH (oids = false);


DROP TABLE IF EXISTS "promotions_artists";
DROP SEQUENCE IF EXISTS promotions_artists_id_seq;
CREATE SEQUENCE promotions_artists_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."promotions_artists" (
    "id" bigint DEFAULT nextval('promotions_artists_id_seq') NOT NULL,
    "promotions_id" bigint,
    "artists" bigint,
    "inserted_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL,
    CONSTRAINT "promotions_artists_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "promotions_artists_artists_fkey" FOREIGN KEY (artists) REFERENCES artists(id) NOT DEFERRABLE,
    CONSTRAINT "promotions_artists_promotions_id_fkey" FOREIGN KEY (promotions_id) REFERENCES promotions(id) NOT DEFERRABLE
) WITH (oids = false);


DROP TABLE IF EXISTS "schema_migrations";
CREATE TABLE "public"."schema_migrations" (
    "version" bigint NOT NULL,
    "inserted_at" timestamp,
    CONSTRAINT "schema_migrations_pkey" PRIMARY KEY ("version")
) WITH (oids = false);

INSERT INTO "schema_migrations" ("version", "inserted_at") VALUES
(20180704202507,	'2018-07-04 20:25:33.803'),
(20180709135250,	'2018-07-09 14:09:50.303'),
(20180709141014,	'2018-07-09 14:31:22.695'),
(20180709144926,	'2018-07-09 14:55:13.574'),
(20180709171104,	'2018-07-09 17:15:04.146'),
(20180709172733,	'2018-07-09 17:41:15.301'),
(20180709175040,	'2018-07-09 17:52:54.954'),
(20180722184831,	'2018-07-22 18:49:57.782'),
(20180722185207,	'2018-07-22 18:53:04.545'),
(20180724181538,	'2018-07-24 18:16:42.385'),
(20180724182225,	'2018-07-24 18:31:15.281'),
(20180806210633,	'2018-08-06 21:15:02.433'),
(20181001203816,	'2018-10-01 20:46:11.825'),
(20181001205230,	'2018-10-09 18:45:28.866'),
(20181009184012,	'2018-10-09 18:45:29.567'),
(20181203205105,	'2018-12-03 21:09:47.403'),
(20181203211219,	'2018-12-03 21:16:01.613'),
(20190225211836,	'2019-02-25 21:22:24.508'),
(20190320185648,	'2019-03-20 19:09:28.906'),
(20200328152519,	'2020-03-28 15:45:10.726641');

-- 2020-03-28 16:35:39.037228+00
