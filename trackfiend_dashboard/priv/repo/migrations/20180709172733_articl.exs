defmodule TrackfiendApi.Repo.Migrations.Articl do
  use Ecto.Migration

  def change do
    create table(:articles) do
      add :title, :string
      add :body, :string
      add :url, :string
      add :summary, :string
      add :article_type, references(:article_type)

      timestamps()
    end
  end
end
