defmodule TrackfiendApi.Repo.Migrations.DropUnnecessaryArticlesArtistsConstraints do
  use Ecto.Migration

  def change do
    execute "ALTER TABLE artists_articles DROP CONSTRAINT artists_articles_articles_id_fkey"
    execute "ALTER TABLE artists_articles DROP CONSTRAINT artists_articles_artists_id_fkey"
  end
end
