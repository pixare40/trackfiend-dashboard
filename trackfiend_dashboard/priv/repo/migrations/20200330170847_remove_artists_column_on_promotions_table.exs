defmodule TrackfiendApi.Repo.Migrations.RemoveArtistsColumnOnPromotionsTable do
  use Ecto.Migration

  def change do
    alter table(:promotions) do
      remove(:artists)
    end
  end
end
