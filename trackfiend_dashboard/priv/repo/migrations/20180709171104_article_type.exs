defmodule TrackfiendApi.Repo.Migrations.ArticleType do
  use Ecto.Migration

  def change do
    create table(:article_type) do
      add :article_type, :string

      timestamps()
    end
  end
end
