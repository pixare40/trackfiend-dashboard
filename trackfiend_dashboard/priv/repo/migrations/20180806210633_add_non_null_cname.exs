defmodule TrackfiendApi.Repo.Migrations.AddNonNullCname do
  use Ecto.Migration

  def change do
    alter table(:artists) do
      modify :cname, :string, null: false
    end
  end
end
