defmodule TrackfiendApi.Repo.Migrations.DropUniqueUnusableIndices do
  use Ecto.Migration

  def change do
    drop index(:artists, [:cname, :mbid, :discogsid], name: :artists_unique_indexes)
    drop index(:artists, [:cname, :mbid, :discogsid], name: :artists_cname_mbid_discogsid_index)

    create unique_index(:artists, [:cname])
  end
end
