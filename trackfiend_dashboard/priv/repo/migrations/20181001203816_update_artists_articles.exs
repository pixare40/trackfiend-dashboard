defmodule TrackfiendApi.Repo.Migrations.UpdateArtistsArticles do
  use Ecto.Migration

  def change do
    rename table(:artists_articles), :artists_id, to: :artist_id
    rename table(:artists_articles), :articles_id, to: :article_id
  end
end
