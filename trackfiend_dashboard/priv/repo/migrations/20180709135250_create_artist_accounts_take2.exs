defmodule TrackfiendApi.Repo.Migrations.CreateArtistAccountsTake2 do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :firstname, :string
      add :lastname, :string
      add :email, :string
      add :password_digest, :string

      timestamps()
    end
  end
end
