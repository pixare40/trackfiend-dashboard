defmodule TrackfiendApi.Repo.Migrations.AddArticlePicture do
  use Ecto.Migration

  def change do
    alter table(:articles) do
      add :article_picture, :string, null: true
    end
  end
end
