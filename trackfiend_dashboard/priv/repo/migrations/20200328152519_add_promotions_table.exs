defmodule TrackfiendApi.Repo.Migrations.AddPromotionsTable do
  use Ecto.Migration

  def change do
    create table(:promotions) do
      add :promotion_name, :string
      add :primary_text, :string
      add :secondary_text, :string
      add :promotion_image, :string
      add :artists, references(:artists)

      timestamps()
    end

    create table(:promotions_artists) do
      add :promotions_id, references(:promotions)
      add :artists, references(:artists)

      timestamps()
    end
  end
end
