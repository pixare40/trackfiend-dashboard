defmodule TrackfiendApi.Repo.Migrations.ChangePromotionsArtistsColumnName do
  use Ecto.Migration

  def change do
    rename table(:promotions_artists), :artists, to: :artist_id
  end
end
