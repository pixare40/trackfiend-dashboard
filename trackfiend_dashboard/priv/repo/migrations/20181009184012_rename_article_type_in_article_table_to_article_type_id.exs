defmodule TrackfiendApi.Repo.Migrations.RenameArticleTypeInArticleTableToArticleTypeId do
  use Ecto.Migration

  def change do
    rename table("articles"), :article_type, to: :article_type_id
  end
end
