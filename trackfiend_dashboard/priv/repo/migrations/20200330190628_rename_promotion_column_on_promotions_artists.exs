defmodule TrackfiendApi.Repo.Migrations.RenamePromotionColumnOnPromotionsArtists do
  use Ecto.Migration

  def change do
    rename table(:promotions_artists), :promotions_id, to: :promotion_id
  end
end
