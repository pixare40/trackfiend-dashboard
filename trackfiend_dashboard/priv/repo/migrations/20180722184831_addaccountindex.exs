defmodule TrackfiendApi.Repo.Migrations.Addaccountindex do
  use Ecto.Migration

  def change do
    create unique_index(:accounts, [:email])
  end
end
