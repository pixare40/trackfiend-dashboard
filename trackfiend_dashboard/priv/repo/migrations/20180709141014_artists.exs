defmodule TrackfiendApi.Repo.Migrations.Artists do
  use Ecto.Migration

  def change do
    create table(:artists) do
      add :name, :string
      add :realname, :string
      add :discogsid, :string
      add :mbid, :string
      add :cname, :string
      add :bio, :string
      add :socials, :string
      add :artist_image_url, :string

      timestamps()
    end
  end
end
