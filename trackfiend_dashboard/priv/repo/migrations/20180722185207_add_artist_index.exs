defmodule TrackfiendApi.Repo.Migrations.AddArtistIndex do
  use Ecto.Migration

  def change do
    create unique_index(:artists, [:cname, :mbid, :discogsid])
  end
end
