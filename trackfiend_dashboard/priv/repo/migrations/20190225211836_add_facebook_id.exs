defmodule TrackfiendApi.Repo.Migrations.AddFacebookId do
  use Ecto.Migration

  def change do
    alter table(:accounts) do
      add :facebook_id, :string, null: true
      add :avatar, :string, null: true
    end
  end
end
