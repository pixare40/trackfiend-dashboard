defmodule TrackfiendApi.Repo.Migrations.AddOnDeletionToArtistsArticles do
  use Ecto.Migration

  def change do
    alter table(:artists_articles) do
      modify :artist_id, references(:artists, on_delete: :delete_all, on_replace: :delete)
      modify :article_id, references(:articles, on_delete: :delete_all, on_replace: :delete)
    end
  end
end
