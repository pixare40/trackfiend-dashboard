defmodule TrackfiendApi.Repo.Migrations.AccountsArtists do
  use Ecto.Migration

  def change do
    create table(:accounts_artists) do
      add :account_id, references(:accounts)
      add :artist_id, references(:artists)

      timestamps()
    end
  end
end
