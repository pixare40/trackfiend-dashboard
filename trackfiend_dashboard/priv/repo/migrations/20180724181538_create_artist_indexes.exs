defmodule TrackfiendApi.Repo.Migrations.CreateArtistIndexes do
  use Ecto.Migration

  def change do
    create unique_index(:artists, [:cname, :mbid, :discogsid], name: :artists_unique_indexes)
  end
end
