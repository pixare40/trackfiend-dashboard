defmodule TrackfiendApi.Repo.Migrations.ArtistsArticles do
  use Ecto.Migration

  def change do
    create table(:artists_articles) do
      add :artists_id, references(:artists)
      add :articles_id, references(:articles)

      timestamps()
    end
  end
end
