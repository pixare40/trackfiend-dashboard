defmodule TrackfiendApi.Mixfile do
  use Mix.Project

  def project do
    [
      app: :trackfiend_api,
      version: "0.0.1",
      elixir: "~> 1.9",
      elixirc_paths: elixirc_paths(Mix.env),
      compilers: [:phoenix, :gettext] ++ Mix.compilers,
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {TrackfiendApi, []},
      extra_applications: [:phoenix, :phoenix_pubsub, :phoenix_html, :logger, :gettext,
                    :phoenix_ecto, :postgrex, :comeonin, :hackney, :amqp, :ecto_sql,
                    :jason, :mongodb_driver]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_),     do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [{:phoenix, "~> 1.4.0"},
     {:phoenix_pubsub, "~> 1.0"},
     {:postgrex, ">= 0.0.0"},
     {:phoenix_html, "~> 2.14"},
     {:phoenix_live_reload, "~> 1.2", only: :dev},
     {:gettext, "~> 0.11"},
     {:guardian, "~> 2.1"},
     {:absinthe, "~> 1.5"},
     {:absinthe_plug, "~> 1.5"},
     {:absinthe_phoenix, "~> 1.4"},
     {:dataloader, "~> 1.0.0"},
     {:facebook, "~> 0.24.0", override: true},
     {:hackney, "~> 1.9"},
     {:bcrypt_elixir, "~> 2.0"},
     {:amqp, "~> 1.4"},
     {:plug_cowboy, "~> 2.3"},
     {:plug, "~> 1.7"},
     {:ecto_sql, "~> 3.0"},
     {:phoenix_ecto, "~> 4.0"},
     {:jason, "~> 1.2"},
     {:poison, "~> 3.0"},
     {:httpoison, "~> 1.7"},
     {:scrivener_ecto, "~> 2.4"},
     {:mongodb_driver, "~> 0.7.0"}]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    ["ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
     "ecto.reset": ["ecto.drop", "ecto.setup"],
     test: ["ecto.create --quiet", "ecto.migrate", "test"]]
  end
end
